﻿using JetBrains.Annotations;
using UnityEngine;

namespace Camera
{
    [UsedImplicitly]
    public class Cinematic : MonoBehaviour
    {
        public float BeforeDelay;

        public GameObject TargetGameObject;

        public float AfterDelay;
    }
}
