﻿using System.Collections;
using JetBrains.Annotations;
using Level;
using Other;
using Player;
using UnityEngine;
using UnityEngine.Events;

namespace Camera
{
    [RequireComponent(typeof(UnityEngine.Camera))]
    public class CinematicCamera : MonoBehaviour
    {
        public float Delay = 1f;

        public float Speed = 1f;

        public UnityEvent OnStartCinematic;

        public UnityEvent OnEndCinematic;

        public UnityEvent OnStartMultiCinematic;

        public UnityEvent OnEndMultiCinematic;

        public bool PlayerFreeze = true;

        private PlayerMovement playerMovement;

        private Timer levelTimer;

        private FollowCamera followCameraScript;

        private void Start()
        {
            if (OnStartCinematic == null)
                OnStartCinematic = new UnityEvent();
            if (OnEndCinematic == null)
                OnEndCinematic = new UnityEvent();

            if (PlayerFreeze)
            {
                OnStartCinematic.AddListener(FreezePlayer);
                OnEndCinematic.AddListener(UnfreezePlayer);
                OnStartMultiCinematic.AddListener(FreezePlayer);
                OnEndMultiCinematic.AddListener(UnfreezePlayer);
            }

            OnStartCinematic.AddListener(PauseTimer);
            OnEndCinematic.AddListener(ResumeTimer);

            playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
            levelTimer = GameObject.FindGameObjectWithTag("LevelControl").GetComponent<Timer>();
            followCameraScript = GetComponent<FollowCamera>();
        }

        private void PauseTimer()
        {
            levelTimer.StopTimer();
        }

        private void ResumeTimer()
        {
            levelTimer.StartTimer();
        }

        private void FreezePlayer()
        {
            playerMovement.Freeze(true);
            playerMovement.enabled = false;
        }

        private void UnfreezePlayer()
        {
            playerMovement.Unfreeze();
            playerMovement.enabled = true;
        }

        [UsedImplicitly]
        public void CinematicOn(GameObject go)
        {
            Vector3 targetPosition = go.transform.position.Z(gameObject.transform.position.z);
            StartCoroutine(MoveToAndBack(targetPosition, Delay));
        }

        [UsedImplicitly]
        public void CinematicOnMultiple(CinematicCollection cinematicCollection)
        {
            StartCoroutine(CinematicOnMultipleCoroutine(cinematicCollection));
        }

        private IEnumerator CinematicOnMultipleCoroutine(CinematicCollection cinematicCollection)
        {
            OnStartMultiCinematic.Invoke();

            Vector3 initialPosition = gameObject.transform.position;

            if (followCameraScript)
                followCameraScript.enabled = false;

            foreach (Cinematic cinematic in cinematicCollection)
            {
                yield return new WaitForSecondsRealtime(cinematic.BeforeDelay);
                yield return MoveTo(cinematic.TargetGameObject.transform.position.Z(gameObject.transform.position.z));
                yield return new WaitForSecondsRealtime(cinematic.AfterDelay);
            }

            yield return MoveTo(initialPosition);

            if (followCameraScript)
                followCameraScript.enabled = true;

            OnEndMultiCinematic.Invoke();
        }

        private IEnumerator MoveTo(Vector3 position)
        {
            Vector3 startingPosition = gameObject.transform.position;
            float t = 0;
            while (Mathf.Abs((position - gameObject.transform.position).magnitude) > Mathf2.Epsilon)
            {
                t += Time.deltaTime * Speed;
                gameObject.transform.position = Vector3.Lerp(startingPosition, position, t);
                yield return null;
            }
        }

        private IEnumerator MoveToAndBack(Vector3 position, float delay)
        {
            Vector3 initialPosition = gameObject.transform.position;

            if (followCameraScript)
                followCameraScript.enabled = false;

            OnStartCinematic.Invoke();

            yield return MoveTo(position);
            yield return new WaitForSecondsRealtime(delay);
            yield return MoveTo(initialPosition);

            OnEndCinematic.Invoke();

            followCameraScript.enabled = true;
        }
    }
}
