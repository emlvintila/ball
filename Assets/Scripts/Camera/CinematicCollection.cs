﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Camera
{
    public class CinematicCollection : MonoBehaviour, IEnumerable<Cinematic>
    {
        public IEnumerable<Cinematic> Cinematics => cinematics;

#pragma warning disable IDE0044 // Add readonly modifier
        [SerializeField]
        private Cinematic[] cinematics;
#pragma warning restore IDE0044 // Add readonly modifier

        public IEnumerator<Cinematic> GetEnumerator()
        {
            return ((IEnumerable<Cinematic>) cinematics).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return cinematics.GetEnumerator();
        }
    }
}
