﻿using System;
using UnityEngine;

namespace Camera
{
    [RequireComponent(typeof(UnityEngine.Camera))]
    public class FollowCamera : MonoBehaviour
    {
        public GameObject GameObjectToFollow;

        [NonSerialized]
        public Vector3 Delta;

        public Vector3 InitialDelta { get; private set; }

        private void Start()
        {
            if (!GameObjectToFollow)
                GameObjectToFollow = GameObject.FindGameObjectWithTag("Player");
            Delta = transform.position - GameObjectToFollow.transform.position;
            InitialDelta = Delta;
        }

        private void LateUpdate()
        {
            transform.position = GameObjectToFollow.transform.position + Delta;
        }
    }
}
