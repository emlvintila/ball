﻿using UnityEngine;

namespace Camera
{
    [RequireComponent(typeof(UnityEngine.Camera))]
    public class VelocityFov : MonoBehaviour
    {
        public Rigidbody Rigidbody;

        public float VelocityMultiplier = 1f / 40;

        public float InterpolationSpeed = 1f;

        private new UnityEngine.Camera camera;

        private float initialFov;

        private float targetFov;

        private void Start()
        {
            if (!Rigidbody)
                Rigidbody = GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody>();
            camera = GetComponent<UnityEngine.Camera>();
            initialFov = camera.fieldOfView;
        }

        private void FixedUpdate()
        {
            targetFov = initialFov * (1 + Rigidbody.velocity.magnitude * VelocityMultiplier);
            camera.fieldOfView = Mathf.Lerp(camera.fieldOfView, targetFov, Time.deltaTime * InterpolationSpeed);
        }
    }
}
