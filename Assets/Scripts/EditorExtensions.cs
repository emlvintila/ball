﻿using System.IO;
using UnityEditor;
using UnityEngine;

public class EditorExtensions
{
#if UNITY_EDITOR
    [MenuItem("Tools/Clear Progress")]
    public static void ClearProgress()
    {
        string path = Path.Combine(Application.persistentDataPath, "Progress.dat");
        File.Delete(path);
    }
#endif
}