﻿using UnityEngine;
using UnityEngine.Events;

namespace Environment.Abstracts
{
    public abstract class Breakable : EnvironmentalObject
    {
        public float NeededForce;

        public UnityEvent OnBroken;

        protected virtual void OnCollisionEnter(Collision collision)
        {

        }
    }
}
