﻿using UnityEngine;

namespace Environment.Abstracts
{
    public abstract class EnvironmentalObject : MonoBehaviour
    {
        protected EnvironmentalObjectSkin Skin;

        protected void Awake()
        {
            Skin = GetComponent<EnvironmentalObjectSkin>();
            if (!Skin && EnvironmentalObjectSkin.Dict.ContainsKey(GetType()))
                Skin = (EnvironmentalObjectSkin) gameObject.AddComponent(EnvironmentalObjectSkin.Dict[GetType()]);
        }
    }
}
