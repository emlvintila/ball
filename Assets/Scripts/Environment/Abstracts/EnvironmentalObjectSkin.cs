﻿using System;
using System.Collections.Generic;
using Environment.Skins;
using Player;
using UnityEngine;

namespace Environment.Abstracts
{
    [RequireComponent(typeof(Renderer))]
    public abstract class EnvironmentalObjectSkin : MonoBehaviour
    {
        [SerializeField]
        protected int SelfSkinId;

        protected int TargetSkinId;

        protected Renderer Renderer;

        protected ProgressSaver ProgressSaver;

        protected void Awake()
        {
            Renderer = GetComponent<Renderer>();
            ProgressSaver = FindObjectOfType<ProgressSaver>();
            TargetSkinId = ProgressSaver.PlayerProgress.GetSelectedSkinId(GetType());
            SetSkin();
        }

        protected void SetSkin()
        {
            if (SelfSkinId != TargetSkinId)
            {
                Material material = Resources.Load<Material>(GetMaterialPath());
                Renderer.material = material;
                SelfSkinId = TargetSkinId;
            }
        }

        protected string GetMaterialPath()
        {
            return $"Materials/Skins/{GetType().Name}/{TargetSkinId}";
        }

        public static readonly Dictionary<Type, Type> Dict = new Dictionary<Type, Type>
        {
            {
                typeof(Elevator),
                typeof(ElevatorSkin)
            },
            {
                typeof(Gate),
                typeof(GateSkin)
            },
            {
                typeof(Spikes),
                typeof(SpikesSkin)
            },
            {
                typeof(Spring),
                typeof(SpringSkin)
            }
        };
    }
}
