﻿using UnityEngine;

namespace Environment.Abstracts
{
    public abstract class Trap : EnvironmentalObject
    {
        protected abstract void OnCollisionEnter(Collision collision);

        public abstract void Kill(GameObject objectToKill);
    }
}
