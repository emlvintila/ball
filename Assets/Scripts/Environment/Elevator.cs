﻿using System.Collections;
using System.Linq;
using Environment.Abstracts;
using UnityEngine;

namespace Environment
{
    [RequireComponent(typeof(Animation))]
    public class Elevator : EnvironmentalObject
    {
        private Vector3 fromPos;

        private Vector3 toPos;

        public Transform To;

        public float Delay;

        public float Duration;

        private new Animation animation;

        private void Start()
        {
            fromPos = transform.position;
            toPos = To.position;

            animation = GetComponent<Animation>();
            GenerateMoveAnimation(toPos - fromPos);

            StartCoroutine(Move());
        }

        private void GenerateMoveAnimation(Vector3 delta)
        {
            Vector3 target = new Vector3(
                                 delta.x * transform.localScale.x * (1 / transform.lossyScale.x),
                                 delta.y * transform.localScale.y * (1 / transform.lossyScale.y),
                                 delta.z * transform.localScale.z * (1 / transform.lossyScale.z)
                             ) + transform.localPosition;

            float start0 = Delay;
            float end0 = start0 + Duration;

            float start1 = end0 + Delay;
            float end1 = start1 + Duration;

            AnimationCurve xCurve = AnimationCurve.Linear(start0, transform.localPosition.x, end0, target.x);
            AnimationCurve yCurve = AnimationCurve.Linear(start0, transform.localPosition.y, end0, target.y);
            AnimationCurve zCurve = AnimationCurve.Linear(start0, transform.localPosition.z, end0, target.z);

            AnimationCurve xCurveReturn = AnimationCurve.Linear(start1, target.x, end1, transform.localPosition.x);
            AnimationCurve yCurveReturn = AnimationCurve.Linear(start1, target.y, end1, transform.localPosition.y);
            AnimationCurve zCurveReturn = AnimationCurve.Linear(start1, target.z, end1, transform.localPosition.z);

            AnimationCurve xCurveTotal = new AnimationCurve(xCurve.keys.Concat(xCurveReturn.keys).ToArray());
            AnimationCurve yCurveTotal = new AnimationCurve(yCurve.keys.Concat(yCurveReturn.keys).ToArray());
            AnimationCurve zCurveTotal = new AnimationCurve(zCurve.keys.Concat(zCurveReturn.keys).ToArray());

            AnimationClip clip = new AnimationClip { legacy = true, wrapMode = WrapMode.Once };
            clip.SetCurve("", typeof(Transform), "localPosition.x", xCurveTotal);
            clip.SetCurve("", typeof(Transform), "localPosition.y", yCurveTotal);
            clip.SetCurve("", typeof(Transform), "localPosition.z", zCurveTotal);

            animation.AddClip(clip, "Move");
        }

        private IEnumerator Move()
        {
            while (true)
            {
                while (animation.IsPlaying("Move"))
                {
                    yield return null;
                }
                animation.Play("Move");
            }
        }
    }
}
