﻿using Environment.Abstracts;
using UnityEngine;

namespace Environment
{
    public class Explosive : Breakable
    {
        public float ExplosionForce;

        public float ExplosionRadius;

        private new Rigidbody rigidbody;

        private void Start()
        {
            rigidbody = GetComponent<Rigidbody>();
        }

        protected override void OnCollisionEnter(Collision collision)
        {
            base.OnCollisionEnter(collision);
            if (!collision.rigidbody) return;
            Vector3 contactPoint = collision.contacts[0].point;
            float force = ExplosionForce * collision.relativeVelocity.magnitude;
            collision.rigidbody.AddExplosionForce(force, contactPoint, ExplosionRadius);
            rigidbody.AddExplosionForce(force, contactPoint, ExplosionRadius);
        }
    }
}
