﻿using System.Collections;
using System.Collections.Generic;
using Environment.Abstracts;
using UnityEngine;
using UnityEngine.Events;

namespace Environment
{
    [RequireComponent(typeof(Animator))]
    public class Gate : EnvironmentalObject
    {
        public AudioClip FinishSound;

        public AudioClip ProcessSound;

        public AudioSource AudioSource;

        public float Duration
        {
            get { return duration; }
            set
            {
                duration = value;
                animator.SetFloat(speedHash, 1 / duration);
            }
        }

        // before the first frame
        public UnityEvent OnBeforeBeginOpen;

        // first frame
        public UnityEvent OnBeforeOpen;

        // last frame
        public UnityEvent OnAfterOpen;

        public UnityEvent OnBeforeBeginClose;

        public UnityEvent OnBeforeClose;

        public UnityEvent OnAfterClose;

        public float Delay;

        [SerializeField]
        private float duration;

        private Animator animator;

        private readonly int speedHash = Animator.StringToHash("Speed");

        private readonly int openHash = Animator.StringToHash("Open");

        private readonly int closeHash = Animator.StringToHash("Close");

        private void Start()
        {
            if (!AudioSource)
                AudioSource = GameObject.FindGameObjectWithTag("LevelControl").GetComponent<AudioSource>();
            animator = GetComponent<Animator>();
            Duration = duration;

            OnBeforeOpen.AddListener(PlayBeginSound);
            OnBeforeClose.AddListener(PlayBeginSound);
            OnAfterOpen.AddListener(PlayFinishSound);
            OnAfterClose.AddListener(PlayFinishSound);
        }

        public void Open()
        {
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("GateOpen"))
                StartCoroutine(OpenCoroutine());
        }

        private IEnumerator OpenCoroutine()
        {
            OnBeforeBeginOpen.Invoke();
            yield return new WaitForSecondsRealtime(Delay);
            animator.ResetTrigger(closeHash);
            animator.SetTrigger(openHash);
        }

        public void Close()
        {
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("GateClose"))
                StartCoroutine(CloseCorotuine());
        }

        private IEnumerator CloseCorotuine()
        {
            OnBeforeBeginClose.Invoke();
            yield return new WaitForSecondsRealtime(Delay);
            animator.ResetTrigger(openHash);
            animator.SetTrigger(closeHash);
        }

        public void PlayBeginSound()
        {
            AudioSource.clip = ProcessSound;
            AudioSource.Play();
        }

        public void PlayFinishSound()
        {
            AudioSource.Stop();
            AudioSource.PlayOneShot(FinishSound);
        }

        #region AnimationEventMethods
        private void InvokeOnBeforeClose()
        {
            OnBeforeClose.Invoke();
        }

        private void InvokeOnAfterClose()
        {
            OnAfterClose.Invoke();
        }

        private void InvokeOnBeforeOpen()
        {
            OnBeforeOpen.Invoke();
        }

        private void InvokeOnAfterOpen()
        {
            OnAfterOpen.Invoke();
        }
        #endregion
    }
}
