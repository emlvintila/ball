﻿using Environment.Abstracts;
using UnityEngine;

namespace Environment
{
    public class GlassBarrier : Breakable
    {
        public GameObject GlassShards;
        public AudioClip GlassBreakSound;
        public GameObject levelControl;

        private AudioSource audioSource;

        private void Start()
        {
            audioSource = levelControl?.GetComponent<AudioSource>();
        }

        protected override void OnCollisionEnter(Collision collision)
        {
            base.OnCollisionEnter(collision);
            if (collision.relativeVelocity.magnitude >= NeededForce)
            {
                if (collision.rigidbody)
                {
                    // Maintain the object's velocity
                    collision.rigidbody.velocity = collision.relativeVelocity;
                    GameObject shards = Instantiate(GlassShards, collision.contacts[0].point, Quaternion.identity);

                    if(GlassBreakSound != null)
                    {
                        audioSource?.PlayOneShot(GlassBreakSound);
                    }

                    Destroy(shards, 1f);
                    OnBroken?.Invoke();
                    gameObject.SetActive(false);
                }
            }
        }
    }
}
