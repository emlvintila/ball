﻿using Environment.Abstracts;
using UnityEngine;

namespace Environment
{
    public class Portal : Helpful
    {
        public Transform TeleportTo;

        public Vector3 Direction;

        protected override void OnCollisionEnter(Collision collision)
        {
            base.OnCollisionEnter(collision);
            Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();
            Vector3 velocity = collision.relativeVelocity;
            Vector3 translation =
                new Vector3(Direction.x * velocity.x, Direction.y * velocity.y, Direction.z * velocity.z);
            rb.MovePosition(TeleportTo.position);
            rb.AddForce(translation, ForceMode.VelocityChange);
        }
    }
}
