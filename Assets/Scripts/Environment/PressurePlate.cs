﻿using Environment.Abstracts;
using UnityEngine;
using UnityEngine.Events;

namespace Environment
{
    public class PressurePlate : Helpful
    {
        public UnityEvent OnActivated;

        public UnityEvent OnDeactivated;

        private uint colliders = 0;

        // OnTriggerEnter is called when the Collider other enters the trigger
        private void OnTriggerEnter(Collider other)
        {
            ++colliders;
            if (colliders > 0)
            {
                OnActivated?.Invoke();
            }
        }

        // OnTriggerExit is called when the Collider other has stopped touching the trigger
        private void OnTriggerExit(Collider other)
        {
            --colliders;
            if (colliders <= 0)
            {
                OnDeactivated?.Invoke();
            }
        }
    }
}