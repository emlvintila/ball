﻿using System.Collections;
using Environment.Abstracts;
using Other;
using UnityEngine;
using UnityEngine.Events;

namespace Environment
{
    public class ShurikenThrower : EnvironmentalObject
    {
        public GameObject Shuriken;

        public Vector3 Force = new Vector3(10, 10);

        public Transform InstantiatePosition;

        public float Interval;

        public UnityEvent OnRestarted, OnStopped;

        private bool active = true;
        public bool Active
        {
            get { return active; }
            private set
            {
                active = value;
                if (value)
                {
                    OnRestarted.Invoke();
                }
                else
                {
                    OnStopped.Invoke();
                }
            }
        }

        public bool TargetPlayer = false;

        private GameObject player;

        private Collider playerCollider;

        private void Start()
        {
            if (OnRestarted == null)
            {
                OnRestarted = new UnityEvent();
            }

            if (OnStopped == null)
            {
                OnStopped = new UnityEvent();
            }

            player = GameObject.FindGameObjectWithTag("Player");
            playerCollider = player.GetComponent<Collider>();

            StartCoroutine(LoopThrow());
        }

        private IEnumerator LoopThrow()
        {
            while (true)
            {
                if (Active)
                {
                    GameObject shuriken = Instantiate(Shuriken, InstantiatePosition.position, InstantiatePosition.rotation);
                    Rigidbody rb = shuriken.GetComponent<Rigidbody>();
                    Vector3 force = Force;
                    if (TargetPlayer)
                    {
                        Vector3 direction = player.transform.position - InstantiatePosition.position;
                        Ray ray = new Ray(InstantiatePosition.position, direction);
                        RaycastHit hit;
                        if (Physics.Raycast(ray, out hit))
                        {
                            if (hit.collider.Equals(playerCollider))
                            {
                                force = direction.normalized * Force.magnitude;
                            }
                        }
                    }
                    if (rb)
                        rb?.AddForce(force, ForceMode.Impulse);
                    yield return new WaitForSeconds(Interval);
                }
                else
                {
                    yield return null;
                }
            }
        }

        public void Restart()
        {
            Active = true;
        }

        public void Stop()
        {
            Active = false;
        }
    }
}
