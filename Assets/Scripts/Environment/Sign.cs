﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Environment.Abstracts;
using Other;
using Player;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Environment
{
    public class Sign : Helpful
    {
        private Canvas tooltip;

        private List<Graphic> graphics;

        private readonly List<Coroutine> coroutines = new List<Coroutine>();

        private PlayerMovement playerMovement;

        public float FadeDuration = 1f;

        public UnityEvent OnPlayerEnter;

        public UnityEvent OnPlayerExit;

        private void Start()
        {
            if (!tooltip)
                tooltip = GetComponentInChildren<Canvas>();

            if (!playerMovement)
                playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
            playerMovement.OnDieEvent.AddListener(_ => Fade(0f, 0f));

            graphics = tooltip.GetComponentsInChildren<Graphic>().ToList();

            OnPlayerEnter.AddListener(() => Fade(1f, FadeDuration));
            OnPlayerExit.AddListener(() => Fade(0f, FadeDuration));
        }

        private void Fade(float alpha, float duration)
        {
            foreach (Coroutine coroutine in coroutines)
                StopCoroutine(coroutine);
            coroutines.Clear();

            coroutines.Add(StartCoroutine(FadeCoroutine(alpha, duration)));
        }

        private IEnumerator FadeCoroutine(float alpha, float duration)
        {
#pragma warning disable IDE0039 // Use local function
            // ReSharper disable once ConvertToLocalFunction
            Func<Graphic, bool> predicate = graphic => Mathf.Abs(graphic.color.a - alpha) > Mathf2.Epsilon;
#pragma warning restore IDE0039 // Use local function

            float t = 0f;
            while (graphics.Any(predicate))
            {
                t += Time.deltaTime / duration;
                foreach (Graphic graphic in graphics.Where(predicate))
                    graphic.color = graphic.color.Alpha(Mathf.Lerp(graphic.color.a, alpha, t));
                yield return null;
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.HasTag("Player"))
            {
                OnPlayerEnter.Invoke();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.HasTag("Player"))
            {
                OnPlayerExit.Invoke();
            }
        }

        //private void OnTriggerStay(Collider other)
        //{
        //    throw new System.NotImplementedException();
        //}
    }
}
