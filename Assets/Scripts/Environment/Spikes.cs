﻿using Environment.Abstracts;
using Player;
using UnityEngine;

namespace Environment
{
    public class Spikes : Trap
    {
        protected override void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                Kill(collision.gameObject);
            }
        }

        public override void Kill(GameObject objectToKill)
        {
            objectToKill.GetComponent<PlayerMovement>()?.Die(this);
        }
    }
}
