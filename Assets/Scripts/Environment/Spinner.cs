﻿using Environment.Abstracts;
using Other;
using System;
using UnityEngine;
using UnityEngine.Events;

namespace Environment
{
    public class Spinner : EnvironmentalObject
    {
        public UnityEvent OnStopped;

        public UnityEvent OnStarted;

        public UnityEvent OnInfiniteStarted;

        public float DurationPerSpin = 1;

        public float DegreesPerSpin = 90;

        public bool IsStopped => Mathf.Abs(DegreesRemaining) > Mathf2.Epsilon;

        // Negative values make it spin infinite times
        private float DegreesRemaining
        {
            get
            {
                return degreesRemaining;
            }

            set
            {
                if (IsStopped)
                    OnStarted.Invoke();
                if (Mathf.Abs(value) < Mathf2.Epsilon && !IsStopped)
                    OnStopped.Invoke();
                degreesRemaining = value;
            }
        }

        private float degreesRemaining;

        private void Start()
        {
            Debug.Assert(Math.Abs(DurationPerSpin) > Mathf2.Epsilon);
        }

        public void Spin(float times)
        {
            DegreesRemaining = times * DegreesPerSpin;
        }

        public void SpinInfinite()
        {
            DegreesRemaining = -1;
            OnInfiniteStarted.Invoke();
        }

        public void StopSpinning()
        {
            DegreesRemaining = 0;
        }

        public void StopSpinningNextRotation()
        {
            DegreesRemaining = DegreesRemaining % DegreesPerSpin;
        }

        private void FixedUpdate()
        {
            if (Math.Abs(DegreesRemaining) <= Mathf2.Epsilon)
            {
                return;
            }

            Vector3 targetEuler = transform.rotation.eulerAngles;
            targetEuler.z += DegreesPerSpin;
            Quaternion target = Quaternion.Euler(targetEuler);
            Quaternion newRot = Quaternion.Slerp(transform.rotation, target,
                Time.deltaTime / DurationPerSpin);
            DegreesRemaining -= Quaternion.Angle(transform.rotation, newRot);
            transform.rotation = newRot;
        }
    }
}
