﻿using Environment.Abstracts;
using UnityEngine;

namespace Environment
{
    public class Spring : Helpful
    {
        private AudioSource audioSource;

        public Vector3 PushVector;

        public GameObject levelControl;
        public AudioClip springSound;

        public bool StopBeforePush = true;

        private void Start()
        {
            audioSource = levelControl?.GetComponent<AudioSource>();
        }

        protected override void OnCollisionEnter(Collision collision)
        {
            base.OnCollisionEnter(collision);
            Rigidbody rb = collision.gameObject.GetComponent<Rigidbody>();

            if (!rb) return;

            if (StopBeforePush)
            {
                rb.velocity = Vector3.zero;
                //rb.angularVelocity = Vector3.zero;
            }

            if (springSound != null)
            {
                audioSource?.PlayOneShot(springSound);
            }
            rb.AddForce(PushVector, ForceMode.VelocityChange);
        }
    }
}
