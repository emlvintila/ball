﻿using Events;
using UnityEngine;
using UnityEngine.Events;

namespace Environment
{
    [RequireComponent(typeof(MeshRenderer))]
    public class Toggler : MonoBehaviour
    {
        public BoolEvent OnToggled;

        public UnityEvent OnEnabled;

        public UnityEvent OnDisabled;

        public Material EnabledMaterial;

        public Material DisabledMaterial;

        [SerializeField]
        private bool state;
        public bool State => state;

        private MeshRenderer meshRenderer;

        private uint colliders;

        private void Start()
        {
            if (OnToggled == null)
            {
                OnToggled = new BoolEvent();
            }

            meshRenderer = GetComponent<MeshRenderer>();
            OnToggled.AddListener(ChangeColor);
            OnToggled.AddListener(InvokeSpecific);
            ChangeColor(state);
        }

        private void ChangeColor(bool active)
        {
            meshRenderer.material = active ? EnabledMaterial : DisabledMaterial;
        }

        private void InvokeSpecific(bool newState)
        {
            if (newState)
            {
                OnEnabled.Invoke();
            }
            else
            {
                OnDisabled.Invoke();
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.attachedRigidbody && colliders++ == 0)
            {
                Toggle();
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.attachedRigidbody)
            {
                --colliders;
            }
        }

        public void Toggle()
        {
            state = !state;
            OnToggled.Invoke(state);
        }
    }
}
