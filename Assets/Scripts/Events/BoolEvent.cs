﻿using UnityEngine.Events;

namespace Events
{
    public class BoolEvent : UnityEvent<bool>
    {
    }
}
