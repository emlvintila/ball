﻿using UnityEngine;
using UnityEngine.Events;

namespace Events
{
    public class OnDieEvent : UnityEvent<MonoBehaviour>
    {
    }
}
