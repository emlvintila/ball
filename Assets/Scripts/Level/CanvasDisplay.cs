﻿using System.Collections.Generic;
using Player;
using UnityEngine;

namespace Level
{
    public class CanvasDisplay : MonoBehaviour
    {
        public List<GameObject> GameObjects;

        private PlayerMovement playerMovement;

        private void Start()
        {
            playerMovement = GameObject.FindWithTag("Player")?.GetComponent<PlayerMovement>();

            playerMovement?.OnDieEvent?.AddListener(_ =>
            {
                for (int i = 0; i < GameObjects.Count; ++i)
                    GameObjects[i].SetActive(true);
            });
        }
    }
}
