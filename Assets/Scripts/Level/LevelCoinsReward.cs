﻿using System.Collections.Generic;
using Other;
using Player;
using UI;
using UnityEngine;

namespace Level
{
    public class LevelCoinsReward : MonoBehaviour
    {
        public int Reward;

        private SceneNameHelper sceneNameHelper;

        private ProgressSaver progressSaver;

        private bool awardedOnEnable = false;

        private void Awake()
        {
            sceneNameHelper = sceneNameHelper ??
                              GetComponent<SceneNameHelper>() ??
                              GetComponentInParent<SceneNameHelper>() ??
                              gameObject.AddComponent<SceneNameHelper>();

            progressSaver = GameObject.FindGameObjectWithTag("LevelControl").GetComponent<ProgressSaver>() ??
                            FindObjectOfType<ProgressSaver>();

            Reward = GetReward(sceneNameHelper.SceneName);
        }

        private void OnEnable()
        {
            if (!awardedOnEnable)
            {
                AwardCoins();
                awardedOnEnable = true;
            }
        }

        public void AwardCoins()
        {
            progressSaver.PlayerProgress.Coins += Reward;
        }

        public void DoubleReward()
        {
            AwardCoins();
            Reward *= 2;
        }

        public static readonly IReadOnlyDictionary<string, int> BaseRewards = new Dictionary<string, int>
        {
            {"Tutorial1", 15},
            {"Tutorial2", 20},
            {"Tutorial3", 35},
            {"Level1", 45},
            {"Level2", 30},
            {"Level3", 30},
            {"Level4", 40},
            {"Level5", 30},
            {"Level6", 55},
            {"Level7", 40},
            {"Level8", 60},
            {"Level9", 120},
        };

        public static int GetBaseRewardForLevel(string level)
        {
            if (BaseRewards.ContainsKey(level))
                return BaseRewards[level];

            return 0;
        }

        public static int GetReward(string level)
        {
            // [0.5, 1.5) * reward
            return (int)((Random.value + 0.5f) * GetBaseRewardForLevel(level));
        }
    }
}
