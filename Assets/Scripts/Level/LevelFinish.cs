﻿using Other;
using Player;
using UnityEngine;
using UnityEngine.Events;

namespace Level
{
    public class LevelFinish : MonoBehaviour
    {
        public UnityEvent OnLevelFinished;

        private PlayerMovement playerMovement;

        private void Start()
        {
            if (OnLevelFinished == null)
                OnLevelFinished = new UnityEvent();

            playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();

            OnLevelFinished.AddListener(() => playerMovement.Freeze(true));
            OnLevelFinished.AddListener(() => playerMovement.enabled = false);
        }

        private void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.HasTag("Player"))
                OnLevelFinished.Invoke();
        }
    }
}
