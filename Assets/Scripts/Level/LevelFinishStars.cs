﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Level
{
    public class LevelFinishStars : MonoBehaviour
    {
        public bool AutoUpdateStars = false;

        public Image[] Stars;

        public Sprite EmptyStar;

        public Sprite FullStar;

        public int EmptyStars => Mathf.Clamp((int)(GetTimerDeviation() * Stars.Length), 0, Stars.Length);

        public int FullStars => Stars.Length - EmptyStars;

        [SerializeField]
        private Timer timer;

        private void Start()
        {
            if (!timer)
                timer = GetComponent<Timer>() ?? FindObjectOfType<Timer>();
            if (AutoUpdateStars)
                StartCoroutine(AutoUpdateStarsCoroutine());
        }

        public void UpdateStars()
        {
            for (int i = 0; i < FullStars; ++i)
                Stars[i].sprite = FullStar;

            for (int i = FullStars; i < Stars.Length; ++i)
                Stars[i].sprite = EmptyStar;
        }

        private IEnumerator AutoUpdateStarsCoroutine()
        {
            int lastEmptyStars = -1;
            while (true)
            {
                if (lastEmptyStars != EmptyStars)
                {
                    lastEmptyStars = EmptyStars;
                    UpdateStars();
                }

                yield return null;
            }
        }

        private float GetTimerDeviation()
        {
            return timer?.GetDeviationFromReference() ?? 0;
        }
    }
}
