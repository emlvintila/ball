﻿using Player;
using UnityEngine;
using UnityEngine.UI;

namespace Level
{
    public class LevelFinshTime : MonoBehaviour
    {
        public ProgressSaver ProgressSaver;

        public Text CurrentTime;

        public Text BestTime;

        public void UpdateTime(string level)
        {
            float current = GetComponent<Timer>()?.GetElapsedSeconds() ?? Mathf.Infinity;
            float best = ProgressSaver.PlayerProgress.GetTimeForLevel(level);
            UpdateTime(current, best);
        }

        private void UpdateTime(float current, float best)
        {
            if (CurrentTime)
                CurrentTime.text = float.IsInfinity(current) ? "N/A" : current.ToString("F1");
            if (BestTime)
                BestTime.text = float.IsInfinity(best) ? "N/A" : best.ToString("F1");
        }
    }
}
