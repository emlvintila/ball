﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Level
{
    public class LevelLoader : MonoBehaviour
    {
        public void LoadScene(string scene)
        {
            StartCoroutine(LoadSceneEnumerator(scene));
        }

        private IEnumerator LoadSceneEnumerator(string scene)
        {
            AsyncOperation op = SceneManager.LoadSceneAsync(scene);
            while (!op.isDone)
            {
                yield return null;
            }
        }
    }
}
