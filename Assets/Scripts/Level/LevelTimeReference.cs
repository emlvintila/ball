﻿namespace Level
{
    public enum LevelTimeReference
    {
        _Template = int.MaxValue,

        Tutorial1 = 15,
        Tutorial2 = 20,
        Tutorial3 = 35,

        Level1 = 45,
        Level2 = 30,
        Level3 = 30,
        Level4 = 40,
        Level5 = 30,
        Level6 = 55,
        Level7 = 40,
        Level8 = 60,
        Level9 = 120
    }
}
