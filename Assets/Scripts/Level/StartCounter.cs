﻿using System;
using System.Collections;
using Player;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Level
{
    [Obsolete("Use StartText instead")]
    public class StartCounter : MonoBehaviour
    {
        public int time = 3;
        public Text text;
        public PlayerMovement PlayerMovement;
        public AudioClip countdownSound;
        public AudioClip startSound;

        public UnityEvent OnCountdownFinish;

        private AudioSource audioSource;

        private void Start()
        {
            if (Application.isEditor)
                time = 0;

            audioSource = GetComponent<AudioSource>();

            if (!PlayerMovement)
                PlayerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
            PlayerMovement.enabled = false;

            OnCountdownFinish.AddListener(Input.ResetInputAxes);

            StartCoroutine(Countdown(time));
        }

        private IEnumerator Countdown(int time)
        {
            while (time > 0)
            {
                text.text = time.ToString();
                if (countdownSound)
                    audioSource?.PlayOneShot(countdownSound);
                yield return new WaitForSeconds(1f);
                --time;
            }

            text.text = "Start";
            if (startSound)
                audioSource?.PlayOneShot(startSound);

            yield return new WaitForSeconds(0.5f);

            PlayerMovement.enabled = true;

            text.text = "";

            OnCountdownFinish.Invoke();
        }
    }
}
