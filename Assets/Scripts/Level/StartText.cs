﻿using UnityEngine;
using UnityEngine.UI;

namespace Level
{
    [RequireComponent(typeof(Text))]
    public class StartText : MonoBehaviour
    {
        private Text text;

        [SerializeField]
        private Timer timer;

        private void Awake()
        {
            if (!text)
                text = GetComponent<Text>();
            timer = timer ?? GameObject.FindGameObjectWithTag("LevelControl").GetComponent<Timer>();
        }

        private void Start()
        {
            Time.timeScale = 0f;
            Time.fixedDeltaTime = 0f;
        }

        private void Update()
        {
            if (gameObject.activeSelf)
                if (Input.GetMouseButton(0))
                    Hide();
        }

        private void OnDisable()
        {
            timer.StartTimer();
        }

        private void Hide()
        {
            if (gameObject.activeSelf)
                gameObject.SetActive(false);
        }
    }
}
