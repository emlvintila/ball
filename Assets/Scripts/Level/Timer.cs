﻿using System;
using System.Collections;
using Other;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Level
{
    public class Timer : MonoBehaviour
    {
        public float ReferenceTime = 0f;

        public bool UseScaledTime = false;

        private float elapsed;

        private bool active = false;

        private void Start()
        {
            StartCoroutine(Coroutine());
            if (Mathf.Abs(ReferenceTime) < Mathf2.Epsilon)
            {
                LevelTimeReference result = (LevelTimeReference) Enum.Parse(typeof(LevelTimeReference), SceneManager.GetActiveScene().name, true);
                ReferenceTime = (float) result;
            }
        }

        private IEnumerator Coroutine()
        {
            while (true)
            {
                if (active)
                {
                    if (UseScaledTime)
                    {
                        elapsed += Time.deltaTime;
                    }
                    else
                    {
                        elapsed += Time.unscaledDeltaTime;
                    }
                }
                yield return null;
            }
        }

        public void StartTimer()
        {
            active = true;
        }

        public void StopTimer()
        {
            active = false;
        }

        public void Reset()
        {
            elapsed = 0;
        }

        public float GetElapsedSeconds()
        {
            return elapsed;
        }

        public float GetDeviationFromReference()
        {
            return GetElapsedSeconds() / ReferenceTime - 1;
        }
    }
}
