﻿using UnityEngine;

namespace Menu
{
    public class ContinuousSpin : MonoBehaviour
    {
        public Transform Transform;

        public float DegreesPerSecond = 60;

        public Vector3 Axis = Vector3.up;

        private void Start()
        {
            if (!Transform)
                Transform = transform;
        }

        private void FixedUpdate()
        {
            Transform.Rotate(Axis, DegreesPerSecond * Time.deltaTime);
        }
    }
}
