﻿using System;
using System.Collections;
using Other;
using Player;
using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
    public abstract class EnvironmentalObjectSkinUnlocker : MonoBehaviour
    {
        [SerializeField]
        protected int Id;

        [SerializeField]
        protected long NeededCoins;

        public long CoinsNeededToUnlock => NeededCoins;

        [SerializeField]
        protected string Tooltip;

        [SerializeField]
        protected GameObject Lock;

        [SerializeField]
        protected GameObject Outline;

        [SerializeField]
        protected Text TooltipText;

        protected Type Type;

        protected ProgressSaver ProgressSaver;

        protected virtual void Awake()
        {
            ProgressSaver =  FindObjectOfType<ProgressSaver>();
            Update();
        }

        protected virtual void Update()
        {
            if (ProgressSaver.PlayerProgress.IsSkinUnlocked(Type, Id))
                DisableLock();
            else
                EnableLock();

            if (ProgressSaver.PlayerProgress.GetSelectedSkinId(Type) == Id)
                EnableOutline();
            else
                DisableOutline();
        }

        private void DisableLock()
        {
            if (Lock.activeSelf)
                Lock.SetActive(false);
        }

        private void EnableLock()
        {
            if (!Lock.activeSelf)
                Lock.SetActive(true);
        }

        private void DisableOutline()
        {
            if (Outline.activeSelf)
                Outline.SetActive(false);
        }

        private void EnableOutline()
        {
            if (!Outline.activeSelf)
                Outline.SetActive(true);
        }

        public void TryUnlock()
        {
            if (ProgressSaver.PlayerProgress.IsSkinUnlocked(Type, Id))
                ProgressSaver.PlayerProgress.SelectSkinFor(Type, Id);
            else if (ProgressSaver.PlayerProgress.Coins >= NeededCoins)
            {
                ProgressSaver.PlayerProgress.UnlockSkinFor(Type, Id);
                ProgressSaver.PlayerProgress.Coins -= NeededCoins;
                ProgressSaver.PlayerProgress.SelectSkinFor(Type, Id);
            }
            else
            {
                return;
                ShowTooltip();
            }
        }

        protected void ShowTooltip()
        {
            StopAllCoroutines();
            StartCoroutine(ShowTooltipCoroutine());
        }

        protected IEnumerator ShowTooltipCoroutine()
        {
            yield return Fade(1f);
            yield return new WaitForSecondsRealtime(1f);
            yield return Fade(0f);
        }

        protected IEnumerator Fade(float alpha, float duration = 0.25f)
        {
            float t = 0;
            float initialAlpha = TooltipText.color.a;
            while (Mathf.Abs(TooltipText.color.a - alpha) > Mathf2.Epsilon)
            {
                t += Time.deltaTime / duration;
                TooltipText.color = TooltipText.color.Alpha(Mathf.Lerp(initialAlpha, alpha, t));
                yield return null;
            }
        }

        protected virtual void Reset()
        {
            Lock = transform.Find("Lock").gameObject;
            Outline = transform.Find("Outline").gameObject;
        }
    }
}
