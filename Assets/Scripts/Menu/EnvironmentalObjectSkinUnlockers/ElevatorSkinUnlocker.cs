﻿using Environment.Skins;

namespace Menu.EnvironmentalObjectSkinUnlockers
{
    public class ElevatorSkinUnlocker : EnvironmentalObjectSkinUnlocker
    {
        protected override void Awake()
        {
            Type = typeof(ElevatorSkin);
            base.Awake();
        }
    }
}
