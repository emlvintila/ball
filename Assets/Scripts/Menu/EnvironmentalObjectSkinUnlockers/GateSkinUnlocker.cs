﻿using Environment.Skins;

namespace Menu.EnvironmentalObjectSkinUnlockers
{
    public class GateSkinUnlocker : EnvironmentalObjectSkinUnlocker
    {
        protected override void Awake()
        {
            Type = typeof(GateSkin);
            base.Awake();
        }
    }
}
