﻿using Environment.Skins;

namespace Menu.EnvironmentalObjectSkinUnlockers
{
    public class SpikesSkinUnlocker : EnvironmentalObjectSkinUnlocker
    {
        protected override void Awake()
        {
            Type = typeof(SpikesSkin);
            base.Awake();
        }
    }
}
