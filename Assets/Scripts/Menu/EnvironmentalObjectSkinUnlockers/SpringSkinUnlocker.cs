﻿using Environment.Skins;

namespace Menu.EnvironmentalObjectSkinUnlockers
{
    public class SpringSkinUnlocker : EnvironmentalObjectSkinUnlocker
    {
        protected override void Awake()
        {
            Type = typeof(SpringSkin);
            base.Awake();
        }
    }
}
