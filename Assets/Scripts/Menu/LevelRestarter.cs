﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class LevelRestarter : MonoBehaviour
    {
        public void RestartLevel(string level)
        {
            StartCoroutine(RestartLevelEnumerator(level));
        }

        private IEnumerator RestartLevelEnumerator(string level)
        {
            AsyncOperation op = SceneManager.LoadSceneAsync(level, LoadSceneMode.Single);
            while (!op.isDone)
            {
                yield return null;
            }
        }
    }
}
