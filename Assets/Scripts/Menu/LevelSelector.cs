﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Menu
{
    public class LevelSelector : Submenu
    {
        public void LoadLevel(string level)
        {
            StartCoroutine(LoadLevelEnumerator(level));
        }

        private IEnumerator LoadLevelEnumerator(string scene)
        {
            AsyncOperation op = SceneManager.LoadSceneAsync(scene, LoadSceneMode.Single);
            while (!op.isDone)
            {
                yield return null;
            }
            gameObject.SetActive(false);
        }

        protected override void Update()
        {
            base.Update();
        }
    }
}
