﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Player;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Menu
{
    public class Roulette : MonoBehaviour
    {
        [SerializeField]
        private Text textUp;

        [SerializeField]
        private Text textMiddle;

        [SerializeField]
        private Text textDown;

        [SerializeField]
        private ProgressSaver progressSaver;

        [SerializeField]
        private Button spinButton;

        [SerializeField]
        private Button gainMoreSpinsButton;

        [SerializeField]
        private Text numberOfSpinsText;

        [SerializeField]
        private Text nextSpinTime;

        public List<RouletteReward> Rewards;

        public bool IsSpinning => spinCoroutine != null;

        public bool CanSpin => progressSaver.PlayerProgress.RouletteSpins > 0;

        private Coroutine spinCoroutine;

        private void Awake()
        {
            if (!progressSaver)
                progressSaver = FindObjectOfType<ProgressSaver>();
        }

        private void Update()
        {
            if (CanSpin)
            {
                if (!spinButton.gameObject.activeSelf)
                    spinButton.gameObject.SetActive(true);
                if (gainMoreSpinsButton.gameObject.activeSelf)
                    gainMoreSpinsButton.gameObject.SetActive(false);
            }
            else
            {
                if (spinButton.gameObject.activeSelf)
                    spinButton.gameObject.SetActive(false);
            }

            numberOfSpinsText.text =
                CanSpin ? $"Spin ({progressSaver.PlayerProgress.RouletteSpins})" : $"No spins left!";

            TimeSpan timeRemaining = progressSaver.AwardSpinTimespan -
                                     (DateTime.Now - progressSaver.PlayerProgress.RouletteSpinsLastUpdateTime);
            nextSpinTime.text =
                $"{timeRemaining.Hours.ToString().PadLeft(2, '0')}:" +
                $"{timeRemaining.Minutes.ToString().PadLeft(2, '0')}:" +
                $"{timeRemaining.Seconds.ToString().PadLeft(2, '0')}";
        }

        private void OnEnable()
        {
            progressSaver.UpdateSpins();
        }

        public void Spin()
        {
            if (!CanSpin)
                return;

            if (spinCoroutine != null)
                StopCoroutine(spinCoroutine);
            spinCoroutine = StartCoroutine(SpinCoroutine());
        }

        public void AwardSpin()
        {
            ++progressSaver.PlayerProgress.RouletteSpins;
        }

        private IEnumerator SpinCoroutine()
        {
            RouletteReward reward = Generate();
            int spins =
                Random.Range(Rewards.Count, Random.Range(2, 5) * Rewards.Count) / Rewards.Count * Rewards.Count +
                Rewards.IndexOf(reward);
            AwardReward(reward);
            --progressSaver.PlayerProgress.RouletteSpins;
            for (int i = 1; i <= spins; ++i)
            {
                textUp.text = $"{Rewards[(i - 1) % Rewards.Count].Coins}";
                textMiddle.text = $"{Rewards[i % Rewards.Count].Coins}";
                textDown.text = $"{Rewards[(i + 1) % Rewards.Count].Coins}";
                yield return new WaitForSecondsRealtime((float) i / spins);
            }

            spinCoroutine = null;
        }

        private RouletteReward Generate()
        {
            // weights is sorted ascending
            int[] weights = new int[Rewards.Count];
            int sum = 0;
            for (int i = 0; i < weights.Length; ++i)
            {
                sum += Rewards[i].Weight;
                weights[i] = sum;
            }

            int rand = Random.Range(0, sum + 1);
            for (int i = 0; i < weights.Length; ++i)
                if (weights[i] >= rand)
                    return Rewards[i];
            
            throw new InvalidOperationException();
        }

        private void AwardReward(RouletteReward rouletteReward)
        {
            progressSaver.PlayerProgress.Coins += rouletteReward.Coins;
            progressSaver.Save();
        }
    }
}
