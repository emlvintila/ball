﻿using System;
using UnityEngine;

namespace Menu
{
    public class RouletteReward : MonoBehaviour, IEquatable<RouletteReward>
    {
        public RouletteRewardType RewardType = RouletteRewardType.Coins;

        public int Coins;

        public int Weight = 1;

        public bool Equals(RouletteReward other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return RewardType == other.RewardType && Coins == other.Coins;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == this.GetType() && Equals((RouletteReward) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                // ReSharper disable NonReadonlyMemberInGetHashCode
                return ((int) RewardType * 397) ^ Coins;
                // ReSharper restore NonReadonlyMemberInGetHashCode
            }
        }
    }
}
