﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Other;
using Player;
using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
    public class Skin : MonoBehaviour
    {
        public GameObject Outline;

        public GameObject Lock;

        public Rigidbody Sphere;

        public float OnSelectedForceMultiplier;

        public Text Tooltip;

        public string TooltipText;

#pragma warning disable CS0649, IDE0044
        [SerializeField]
        private int id;
#pragma warning restore CS0649, IDE0044

        private ProgressSaver progressSaver;

        private static readonly Dictionary<Coroutine, Skin> Coroutines = new Dictionary<Coroutine, Skin>(8);

        private ContinuousSpin spinner;

        private void Start()
        {
            progressSaver = Resources.FindObjectsOfTypeAll<ProgressSaver>().First();
            if (!spinner)
                spinner = GetComponentInChildren<ContinuousSpin>();
        }

        private void Update()
        {
            if (progressSaver.PlayerProgress.CurrentSkin == id && Outline.activeSelf == false)
            {
                Outline.SetActive(true);
            }

            // ReSharper disable once RedundantBoolCompare
            if (progressSaver.PlayerProgress.CurrentSkin != id && Outline.activeSelf == true)
            {
                Outline.SetActive(false);
            }

            if (CanSelectSkin() && Lock.activeSelf)
            {
               Lock.SetActive(false);
            }

            // ReSharper disable once InvertIf
            if (!CanSelectSkin() && !Lock.activeSelf)
            {
                Lock.SetActive(true);
            }

            if (progressSaver.PlayerProgress.CurrentSkin == id && !spinner.enabled)
                spinner.enabled = true;

            if (progressSaver.PlayerProgress.CurrentSkin != id && spinner.enabled)
                spinner.enabled = false;
        }

        private void OnDisable()
        {
            HideTooltip();
        }

        private void SelectSkin()
        {
            if (progressSaver.PlayerProgress.CurrentSkin == id)
            {
                return;
            }
            progressSaver.PlayerProgress.CurrentSkin = id;
            progressSaver.Save();
            if (Sphere.velocity.magnitude <= Mathf2.Epsilon)
            {
                Sphere.AddForce(OnSelectedForceMultiplier * Vector3.up, ForceMode.VelocityChange);
            }

            spinner.enabled = true;
        }

        public void ShowTooltip()
        {
            foreach (KeyValuePair<Coroutine, Skin> item in Coroutines)
            {
                if (item.Key != null)
                {
                    item.Value.StopCoroutine(item.Key);
                }
            }
            Coroutines.Clear();
            HideTooltip();
            Tooltip.text = TooltipText;

            Coroutines.Add(StartCoroutine(FadeInOut(3)), this);
        }

        private IEnumerator FadeInOut(float delay)
        {
            Coroutines.Add(StartCoroutine(FadeTooltip(1)), this);
            yield return new WaitForSeconds(delay);
            Coroutines.Add(StartCoroutine(FadeTooltip(0)), this);
        }

        private IEnumerator FadeTooltip(float alpha)
        {
            float t = 0;
            Color targetColor = Tooltip.color;
            targetColor.a = alpha;
            while (Tooltip.color != targetColor)
            {
                Tooltip.color = Color.Lerp(Tooltip.color, targetColor, t);
                t += Time.deltaTime;
                yield return null;
            }
        }

        private bool CanSelectSkin()
        {
            return progressSaver.PlayerProgress.SkinsUnlocked.Contains(id);
        }

        private void HideTooltip()
        {
            Color c = Tooltip.color;
            c.a = 0;
            Tooltip.color = c;
        }

        public void TrySelectSkin()
        {
            if (CanSelectSkin())
            {
                SelectSkin();
            }
            else
            {
                ShowTooltip();
            }
        }
    }
}

