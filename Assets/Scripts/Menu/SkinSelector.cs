﻿using System.Collections;
using System.Linq;
using Player;
using UI;
using UnityEngine;
using UnityEngine.UI;

namespace Menu
{
    public class SkinSelector : MonoBehaviour
    {
        public Button PreviousButton;

        public Button NextButton;

        public int MinSkinId = 0;

        public int MaxSkinId = 14;

        private int CurrentSkinId;

        public Transform InstantiatePosition;

        public Text Tooltip;

        public Sprite LockedSprite;

        public Sprite SelectedSprite;

        public Image Icon;

        public Button UnlockButton;

        private ProgressSaver progressSaver;

        private GameObject currentSkinObject;

        private Coroutine loadingCoroutine;

        private void Awake()
        {
            progressSaver = FindObjectOfType<ProgressSaver>();
            CurrentSkinId = progressSaver.PlayerProgress.CurrentSkin;
            LoadAndInstantiateSkin(CurrentSkinId);
        }

        private void Update()
        {
            if (CurrentSkinId == MinSkinId)
            {
                if (PreviousButton.interactable)
                    PreviousButton.interactable = false;
            }
            else
            {
                if (!PreviousButton.interactable)
                    PreviousButton.interactable = true;
            }

            if (CurrentSkinId == MaxSkinId)
            {
                if (NextButton.interactable)
                    NextButton.interactable = false;
            }
            else
            {
                if (!NextButton.interactable)
                    NextButton.interactable = true;
            }

            if (progressSaver.PlayerProgress.SkinsUnlocked.Contains(CurrentSkinId))
            {
                if (UnlockButton.interactable)
                    UnlockButton.interactable = false;
            }
            else
            {
                if (progressSaver.PlayerProgress.Coins >= PlayerProgress.GetCostForSkin(CurrentSkinId) &&
                    !UnlockButton.interactable)
                    UnlockButton.interactable = true;
            }

            if (!currentSkinObject && loadingCoroutine == null)
            {
                LoadAndInstantiateSkin(CurrentSkinId);
            }
        }

        public void PreviousSkin() => NavigateSkin(-1);

        public void NextSkin() => NavigateSkin(+1);

        private void NavigateSkin(int offset)
        {
            CancelLoadingAndDestroy();
            CurrentSkinId = Mathf.Clamp(CurrentSkinId + offset, MinSkinId, MaxSkinId);
            if (progressSaver.PlayerProgress.SkinsUnlocked.Contains(CurrentSkinId))
                progressSaver.PlayerProgress.CurrentSkin = CurrentSkinId;
            LoadAndInstantiateSkin(CurrentSkinId);
        }

        private void CancelLoadingAndDestroy()
        {
            if (loadingCoroutine != null)
            {
                StopCoroutine(loadingCoroutine);
                loadingCoroutine = null;
            }
        }

        private void LoadAndInstantiateSkin(int id)
        {
            if (loadingCoroutine == null)
                loadingCoroutine = StartCoroutine(G(id));
        }

        private static string GetPathForId(int id)
        {
            return $"Skins/Player{id + 1}";
        }

        private IEnumerator G(int id)
        {
            ResourceRequest request = Resources.LoadAsync<GameObject>(GetPathForId(id));
            while (!request.isDone)
            {
                yield return new WaitForSecondsRealtime(0.25f);
            }

            GameObject prefab = (GameObject) request.asset;
            if (currentSkinObject)
                Destroy(currentSkinObject);
            currentSkinObject = Instantiate(prefab, InstantiatePosition.position, Quaternion.identity, transform);
            Initialize(currentSkinObject);
            Tooltip.text = PlayerProgress.GetTooltipForSkin(id);
            UnlockButton.onClick.RemoveAllListeners();
            UnlockButton.onClick.AddListener(() => TryUnlockSkin(id));
            UnlockButton.GetComponentInChildren<Text>().text = $"Buy ({PlayerProgress.GetCostForSkin(id)})";
            if (progressSaver.PlayerProgress.SkinsUnlocked.Contains(CurrentSkinId) && Icon.sprite != SelectedSprite)
                Icon.sprite = SelectedSprite;
            if (!progressSaver.PlayerProgress.SkinsUnlocked.Contains(CurrentSkinId) && Icon.sprite != LockedSprite)
                Icon.sprite = LockedSprite;
            loadingCoroutine = null;
        }

        private void TryUnlockSkin(int id)
        {
            int cost = PlayerProgress.GetCostForSkin(id);
            if (cost <= 0)
                return;
            if (cost <= progressSaver.PlayerProgress.Coins && !progressSaver.PlayerProgress.SkinsUnlocked.Contains(id))
            {
                progressSaver.PlayerProgress.Coins -= cost;
                progressSaver.PlayerProgress.SkinsUnlocked =
                    progressSaver.PlayerProgress.SkinsUnlocked.Append(id).ToArray();
                progressSaver.PlayerProgress.CurrentSkin = id;
                Icon.sprite = SelectedSprite;
            }
        }

        private void Initialize(GameObject obj)
        {
            Rigidbody rb = obj.GetComponent<Rigidbody>();
            rb.angularDrag = 0;
            rb.drag = 0;
            Destroy(obj.GetComponent<LineRendererDisplay>());
            Destroy(obj.GetComponent<BreakableEffect>());
            Destroy(obj.GetComponent<PlayerMovement>());
            Destroy(obj.GetComponent<GroundCheck>());
            Destroy(obj.GetComponent<CollisionParticles>());
            // ReSharper disable once LocalVariableHidesMember
            ParticleSystem[] systems = obj.GetComponentsInChildren<ParticleSystem>();
            for (int i = 0; i < systems.Length; i++)
            {
                ParticleSystem particleSystem = systems[i];
                ParticleSystem.MinMaxCurve ss = particleSystem.main.startSize;
                ParticleSystem.MainModule main = particleSystem.main;
                ss.constant *= 12;
                main.startSize = ss;
            }

            ScaleFromRectTransform comp = obj.AddComponent<ScaleFromRectTransform>();
            comp.RectTransform = (RectTransform) InstantiatePosition;
            comp.Multiplier = Vector2.one / 4;
        }
    }
}
