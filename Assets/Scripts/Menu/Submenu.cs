﻿using UnityEngine;
using UnityEngine.Events;

namespace Menu
{
    public class Submenu : MonoBehaviour
    {
        public UnityEvent OnBeforeHide;

        public GameObject Menu;

        private void Start()
        {
            if (OnBeforeHide == null)
                OnBeforeHide = new UnityEvent();
        }

        public void ShowMenu()
        {
            OnBeforeHide.Invoke();
            Menu.SetActive(true);
            gameObject.SetActive(false);
        }

        protected virtual void Update()
        {
            if (Input.GetButton("Cancel"))
            {
                ShowMenu();
            }
        }
    }
}
