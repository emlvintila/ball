﻿using System;
using UnityEngine;

namespace Other
{
    public static class ExtensionMethods
    {
        public static bool HasTag(this GameObject gameObject, string tag)
        {
            return gameObject.CompareTag(tag) || (gameObject.GetComponent<TagCollection>()?.HasTag(tag) ?? false);
        }

        public static Vector3 X(this Vector3 vector3, float x)
        {
            return new Vector3(x, vector3.y, vector3.z);
        }

        public static Vector3 XOffset(this Vector3 vector3, float xOffset)
        {
            return vector3.X(vector3.x + xOffset);
        }

        public static Vector3 Y(this Vector3 vector3, float y)
        {
            return new Vector3(vector3.x, y, vector3.z);
        }

        public static Vector3 YOffset(this Vector3 vector3, float yOffset)
        {
            return vector3.Y(vector3.y + yOffset);
        }

        public static Vector3 Z(this Vector3 vector3, float z)
        {
            return new Vector3(vector3.x, vector3.y, z);
        }

        public static Vector3 ZOffset(this Vector3 vector3, float zOffset)
        {
            return vector3.Z(vector3.z + zOffset);
        }

        public static Vector3 Z0(this Vector3 vector3)
        {
            return vector3.Z(0);
        }

        public static Color Alpha(this Color color, float alpha)
        {
            Color c = color;
            c.a = alpha;

            return c;
        }

        public static Color Maximize(this Color color)
        {
            Color c = color;
            float max = c.maxColorComponent;

            c.r = c.r / max;
            c.g = c.g / max;
            c.b = c.b / max;

            return c;
        }
    }
}
