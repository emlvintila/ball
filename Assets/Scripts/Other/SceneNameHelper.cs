﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Other
{
    public class SceneNameHelper : MonoBehaviour
    {
        public string SceneName;

        private void Awake()
        {
            SceneName = SceneManager.GetActiveScene().name;
        }
    }
}