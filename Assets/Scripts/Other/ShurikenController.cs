﻿using Player;
using UnityEngine;

namespace Other
{
    [RequireComponent(typeof(Rigidbody))]
    public class ShurikenController : MonoBehaviour
    {
        public GameObject DestroyParticles;

        public float Lifetime;

        // private new Rigidbody rigidbody;

        private void Start()
        {
            // rigidbody = GetComponent<Rigidbody>();
            // rigidbody.AddForce(StartForce, ForceMode.Impulse);
            Invoke(nameof(LifetimeEndDefault), Lifetime);
        }

        private void OnCollisionEnter(Collision collision)
        {
            LifetimeEnd(collision.contacts[0].point, Quaternion.identity);
            collision.rigidbody?.GetComponent<PlayerMovement>()?.Die(this);
        }

        private void LifetimeEnd(Vector3 position, Quaternion rotation)
        {
            Instantiate(DestroyParticles, position, rotation);
            Destroy(gameObject);
        }

        private void LifetimeEndDefault() => LifetimeEnd(transform.position, transform.rotation);
    }
}
