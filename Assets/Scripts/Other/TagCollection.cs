﻿using System.Linq;
using UnityEngine;

namespace Other
{
    public class TagCollection : MonoBehaviour
    {
        public string[] Tags;

        public bool HasTag(string tag)
        {
            return Tags.Contains(tag);
        }
    }
}
