﻿using UnityEngine;

public class ParticleReflect : MonoBehaviour
{
    public float ReflectForce = 5f;

    private void OnParticleCollision(GameObject other)
    {
        Rigidbody rb = other.GetComponent<Rigidbody>();
        if (rb)
        {
            Vector3 direction = transform.position - other.transform.position;
            rb.AddForce(direction.normalized * ReflectForce);
        }
    }
}