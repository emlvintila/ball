﻿using System.Collections;
using Environment.Abstracts;
using Other;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Rigidbody), typeof(PlayerMovement))]
    public class BreakableEffect : MonoBehaviour
    {
        public float Distance = 0.5f;

        public float AfterBreakDelay = 0.5f;

        public float Multiplier = 5f;

        public float TargetTimeScale = 0.33f;

        public float ResetTimeScale = 1f;

        public float TargetCameraFov = 90f;

        public float ResetCameraFov = 60f;

        private Breakable breakableToHit;

        private PlayerMovement playerMovement;
       
        private new Rigidbody rigidbody;

        internal Coroutine TimeScaleCoroutine { get; private set; }

        internal Coroutine CameraFovCoroutine { get; private set; }

        private bool doExtraCheck = true;

        private void Start()
        {
            playerMovement = GetComponent<PlayerMovement>();
            rigidbody = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            if (playerMovement.Alive)
            {
                if (!breakableToHit)
                {
                    if (rigidbody.SweepTest(rigidbody.velocity, out RaycastHit hit, Distance))
                    {
                        Breakable breakable = hit.transform.gameObject.GetComponent<Breakable>();
                        if (breakable)
                        {
                            // We're about to hit a breakable
                            breakableToHit = breakable;
                            if (TimeScaleCoroutine != null)
                            {
                                StopCoroutine(TimeScaleCoroutine);
                            }

                            if (CameraFovCoroutine != null)
                            {
                                StopCoroutine(CameraFovCoroutine);
                            }

                            TimeScaleCoroutine = StartCoroutine(SetTimeScale(TargetTimeScale));
                            CameraFovCoroutine = StartCoroutine(SetCameraFov(TargetCameraFov));
                            breakable.OnBroken.AddListener(() =>
                            {
                                doExtraCheck = false;
                                Invoke(nameof(ResetBreakable), AfterBreakDelay);
                            });
                        }
                    }
                }
                else if (doExtraCheck)
                {
                    if (rigidbody.SweepTest(rigidbody.velocity, out RaycastHit hit, Distance))
                    {
                        Breakable breakable = hit.transform.gameObject.GetComponent<Breakable>();
                        if (breakable != breakableToHit)
                        {
                            if (breakable == null)
                            {
                                ResetBreakable();
                            }
                            else
                            {
                                breakableToHit = breakable;
                            }
                        }
                    }
                }
            }
        }

        private void ResetBreakable()
        {
            if (TimeScaleCoroutine != null)
            {
                StopCoroutine(TimeScaleCoroutine);
            }

            if (CameraFovCoroutine != null)
            {
                StopCoroutine(CameraFovCoroutine);
            }

            breakableToHit = null;
            doExtraCheck = true;

            TimeScaleCoroutine = StartCoroutine(SetTimeScale(ResetTimeScale));
            CameraFovCoroutine = StartCoroutine(SetCameraFov(ResetCameraFov));
        }

        private IEnumerator SetTimeScale(float targetTimeScale)
        {
            float t = 0f;
            while (Mathf.Abs(Time.timeScale - targetTimeScale) > Mathf2.Epsilon)
            {
                t +=  Multiplier * Time.deltaTime;
                Time.timeScale = Mathf.Lerp(Time.timeScale, targetTimeScale, t);
                Time.fixedDeltaTime = 0.02f * Time.timeScale;
                yield return null;
            }

            TimeScaleCoroutine = null;
        }

        private IEnumerator SetCameraFov(float fov)
        {
            float t = 0f;
            // ReSharper disable once LocalVariableHidesMember
            UnityEngine.Camera camera = UnityEngine.Camera.main;
            while (Mathf.Abs(camera.fieldOfView - fov) > Mathf2.Epsilon)
            {
                t += Multiplier * Time.deltaTime;
                camera.fieldOfView = Mathf.Lerp(camera.fieldOfView, fov, t);
                yield return null;
            }

            CameraFovCoroutine = null;
        }
    }
}
