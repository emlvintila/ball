﻿using UnityEngine;

namespace Player
{
    public class CollisionParticles : MonoBehaviour
    {
#pragma warning disable IDE0044 // Add readonly modifier
        [SerializeField]
        private float particlesCount;

        [SerializeField]
        private float minForce;

        [SerializeField]
        private new ParticleSystem particleSystem;
#pragma warning restore IDE0044 // Add readonly modifier

        private void OnCollisionEnter(Collision collision)
        {
            float ratio = collision.relativeVelocity.magnitude / minForce;
            // ReSharper disable once InvertIf
            if (ratio >= 1)
            {
                particleSystem.transform.position = collision.contacts[0].point;
                particleSystem.Emit((short) (particlesCount * ratio));
            }
        }
    }
}
