﻿using Other;
using UnityEngine;

namespace Player
{
    public class GroundCheck : MonoBehaviour
    {
        public bool IsGrounded => groundColliders > 0;

        private uint groundColliders;


        private void OnCollisionEnter(Collision collision)
        {
            GameObject go = collision.collider.gameObject;
            groundColliders += go.HasTag("Terrain") ? 1u : 0;
        }

        private void OnCollisionExit(Collision collision)
        {
            GameObject go = collision.collider.gameObject;
            groundColliders -= go.HasTag("Terrain") ? 1u : 0;
        }
    }
}
