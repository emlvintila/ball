﻿using System.Linq;
using Other;
using UnityEngine;
using UnityEngine.UI;

namespace Player
{
    [RequireComponent(typeof(PlayerMovement))]
    public class LineRendererDisplay : MonoBehaviour
    {
        private PlayerMovement playerMovement;

#pragma warning disable CS0649
        [SerializeField]
        private LineRenderer lineRenderer;
#pragma warning restore CS0649

        private readonly Color greenColor = Color.green;

        private readonly Color redColor = Color.red;

        private void Start()
        {
            playerMovement = GetComponent<PlayerMovement>();

            if (!lineRenderer)
                lineRenderer = FindObjectOfType<LineRenderer>();

            playerMovement?.OnDieEvent?.AddListener(_ => ClearLine());

            ClearLine();
            lineRenderer.SetPosition(0, Vector3.zero);
        }

        private void Update()
        {
            if (playerMovement.IsMouseDown)
            {
                Vector3 deltaMovement = playerMovement.DeltaMovement;
                Vector3 endPos = lineRenderer.GetPosition(0) + deltaMovement;
                lineRenderer.positionCount = 2;
                lineRenderer.SetPosition(1, endPos);

                Color color = Color.Lerp(greenColor, redColor, deltaMovement.magnitude / playerMovement.MaxMagnitude)
                    .Maximize();
                //GradientColorKey[] gradientColorKeys = lineRenderer.colorGradient.colorKeys;
                //for (int index = 0; index < gradientColorKeys.Length; index++)
                //    gradientColorKeys[index].color = color;

                //lineRenderer.colorGradient.SetKeys(gradientColorKeys, lineRenderer.colorGradient.alphaKeys);
                lineRenderer.startColor = color;
                lineRenderer.endColor = color;
            }
            else
            {
                ClearLine();
            }
        }

        public void ClearLine()
        {
            lineRenderer.positionCount = 1;
        }
    }
}
