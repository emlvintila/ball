﻿using UnityEngine;

namespace Player
{
    public class PlayerFactory : MonoBehaviour
    {
        private ProgressSaver progressSaver;

        private void Awake()
        {
            progressSaver = FindObjectOfType<ProgressSaver>();
            int skin = progressSaver.PlayerProgress.CurrentSkin;

            string prefabName;
            switch (skin)
            {
                default:
                case SkinDefault:
                    prefabName = "Player1"; 
                    break;
                case SkinDied50:
                    prefabName = "Player2";
                    break;
                case SkinLevelUnder30:
                    prefabName = "Player3";
                    break;
                case SkinGreenLeaves:
                    prefabName = "Player4";
                    break;
                case SkinDumbFace:
                    prefabName = "Player5";
                    break;
                case SkinStars:
                    prefabName = "Player6";
                    break;
                case SkinDynamicLava:
                    prefabName = "Player7";
                    break;
                case SkinBasketball:
                    prefabName = "Player8";
                    break;
                case SkinBeachball:
                    prefabName = "Player9";
                    break;
                case SkinFootball:
                    prefabName = "Player10";
                    break;
                case SkinWheel:
                    prefabName = "Player11";
                    break;
                case SkinGolfball:
                    prefabName = "Player12";
                    break;
                case SkinWood:
                    prefabName = "Player13";
                    break;
                case SkinStone:
                    prefabName = "Player14";
                    break;
                case SkinChristmas:
                    prefabName = "Player15";
                    break;
                case SkinGrass:
                    prefabName = "Player16";
                    break;
                case SkinSnakePattern:
                    prefabName = "Player17";
                    break;
            }

            prefabName = "Skins/" + prefabName;
            GameObject prefab = Resources.Load<GameObject>(prefabName);
            Instantiate(prefab, gameObject.transform.position, Quaternion.identity, null);
        }

        public const int SkinDefault = 0;
        public const int SkinDied50 = 1;
        public const int SkinLevelUnder30 = 2;
        public const int SkinGreenLeaves = 3;
        public const int SkinDumbFace = 4;
        public const int SkinStars = 5;
        public const int SkinDynamicLava = 6;
        public const int SkinBasketball = 7;
        public const int SkinBeachball = 8;
        public const int SkinFootball = 9;
        public const int SkinWheel = 10;
        public const int SkinGolfball = 11;
        public const int SkinWood = 12;
        public const int SkinStone = 13;
        public const int SkinChristmas = 14;
        public const int SkinGrass = 15;
        public const int SkinSnakePattern = 16;
    }
}
