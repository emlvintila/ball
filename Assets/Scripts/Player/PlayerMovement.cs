﻿using Events;
using Other;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Rigidbody), typeof(GroundCheck))]
    public class PlayerMovement : MonoBehaviour
    {
        public GameObject startText;

        public bool IsMouseDown => isMouseDown;

        public bool Alive => alive;

        public float BaseForce => baseForce;

        public float MaxMagnitude;

        public OnDieEvent OnDieEvent;

        public float MaxImpulse = 50f;

        public Vector3 DeltaMovement { get; private set; }

        public GroundCheck GroundCheck { get; private set; }

        public BreakableEffect BreakableEffect { get; private set; }

        private bool isMouseDown = false;

#pragma warning disable CS0649, IDE0044
        [SerializeField]
        private float baseForce;
#pragma warning restore CS0649, IDE0044

        [SerializeField]
        private bool alive = true;

        private Vector3 pressedPosition;

        private new Rigidbody rigidbody;

        private void Awake()
        {
            if (OnDieEvent == null)
                OnDieEvent = new OnDieEvent();
            rigidbody = GetComponent<Rigidbody>();
            GroundCheck = GetComponent<GroundCheck>();
            BreakableEffect = GetComponent<BreakableEffect>();
            Time.timeScale = 1f;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;

            startText = GameObject.FindGameObjectWithTag("StartText");
        }

        private void Update()
        {
            if (!alive)
                return;

            if (Input.GetMouseButtonDown(0))
            {
                DeltaMovement = Vector3.zero;
                pressedPosition = Input.mousePosition;
                isMouseDown = true;
            }

            if (Input.GetMouseButton(0))
            {
                DeltaMovement = Vector3.ClampMagnitude(Input.mousePosition - pressedPosition, MaxMagnitude).Z0();
            }

            if (Input.GetMouseButtonUp(0))
            {
                if (GroundCheck.IsGrounded)
                {
                    Vector3 releasePosition = Input.mousePosition;
                    DeltaMovement = Vector3.ClampMagnitude(releasePosition - pressedPosition, MaxMagnitude).Z0();
                    StopMoving();
                    rigidbody.AddForce(GetComputedForce() * DeltaMovement, ForceMode.Impulse);
                    DeltaMovement = Vector3.zero;
                }

                isMouseDown = false;
            }

            if (GroundCheck.IsGrounded)
            {
                if (BreakableEffect && BreakableEffect.TimeScaleCoroutine != null)
                {
                    BreakableEffect.StopCoroutine(BreakableEffect.TimeScaleCoroutine);
                }
            }

            if (!startText.activeSelf)
            {
                if (GroundCheck.IsGrounded && isMouseDown)
                {
                    Time.timeScale = 0f;
                }
                else
                {
                    Time.timeScale = 1f;
                }
            }

            Time.fixedDeltaTime = 0.02f * Time.timeScale;
        }

        private void OnCollisionStay(Collision collision)
        {
            if (collision.impulse.magnitude > MaxImpulse)
            {
                Die(null);
            }
        }

        public void Die(MonoBehaviour killer)
        {
            if (!alive)
                return;

            Time.timeScale = 0f;
            alive = false;
            isMouseDown = false;
            Freeze(true);
            OnDieEvent.Invoke(killer);
        }

        public float GetComputedForce()
        {
            return baseForce;
        }

        public void StopMoving()
        {
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
        }

        public void Freeze(bool zeroVelocity)
        {
            if (zeroVelocity)
                StopMoving();
            isMouseDown = false;
            rigidbody.isKinematic = true;
            Input.ResetInputAxes();
        }

        public void Unfreeze()
        {
            rigidbody.isKinematic = false;
        }

        public void ResetMouse()
        {
            isMouseDown = false;
        }
    }
}
