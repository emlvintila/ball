﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using Environment.Skins;
using Level;
using UnityEngine;

//#if UNITY_IOS
//using UnityEngine.iOS;
//using CalendarUnit = UnityEngine.iOS.CalendarUnit;
//using NotificationServices = UnityEngine.iOS.NotificationServices;
//using LocalNotification = UnityEngine.iOS.LocalNotification;
//#endif

namespace Player
{
    public class ProgressSaver : MonoBehaviour
    {
        public PlayerProgress PlayerProgress;

        private const int AWARD_SPIN_HOURS_INTERVAL = 23;

        public TimeSpan AwardSpinTimespan { get; } = new TimeSpan(AWARD_SPIN_HOURS_INTERVAL, 0, 0);

        private const string SAVE_FILE = "Progress.dat";
        private string saveFilePath;

        private readonly BinaryFormatter binaryFormatter = new BinaryFormatter();

        private void Awake()
        {
            saveFilePath = Path.Combine(Application.persistentDataPath, SAVE_FILE);
            Load();
        }

        private void Start()
        {
            PlayerMovement player = GameObject.FindGameObjectWithTag("Player")?.GetComponent<PlayerMovement>();
            player?.OnDieEvent?.AddListener(killer => { Died(killer, 1); });
        }

        private void OnDisable()
        {
            Save();
        }

        private void OnDestroy()
        {
            Save();
        }

        public void Save()
        {
            UpdateProgress();

            using (FileStream file = File.Create(saveFilePath))
            {
                binaryFormatter.Serialize(file, PlayerProgress);
            }
        }

        private void Load()
        {
            if (File.Exists(saveFilePath))
            {
                using (FileStream file = File.OpenRead(saveFilePath))
                {
                    try
                    {
                        PlayerProgress = (PlayerProgress)binaryFormatter.Deserialize(file);
                    }
                    catch (Exception ex)
                    {
                        Debug.LogError(ex, this);
                        PlayerProgress = new PlayerProgress();
                    }
                }
            }
            else
            {
                PlayerProgress = new PlayerProgress();
            }

            UpdateProgress();
        }

        public void Died(MonoBehaviour monoBehaviour, int times)
        {
            if (times < 0)
                throw new ArgumentOutOfRangeException(nameof(times));

            for (int i = 0; i < times; ++i)
                PlayerProgress.DiedTo(monoBehaviour == null ? null : monoBehaviour.GetType());
        }

        public void FinishedLevel(string level) =>
            FinishedLevel(level, GetComponent<Timer>()?.GetElapsedSeconds() ?? Mathf.Infinity);

        public void FinishedLevel(string level, float time)
        {
            float levelTime = PlayerProgress.GetTimeForLevel(level);

            if (time < levelTime)
            {
                PlayerProgress.LevelsTimes = PlayerProgress.LevelsTimes.Where(kvp => kvp.Key != level)
                    .Concat(new[] { new KeyValuePair<string, float>(level, time), }).ToList();
            }
        }

        private void UpdateProgress()
        {
            UpdatePlayerSkins();
            UpdateElevatorSkins();
            UpdateGateSkins();
            UpdateSpikesSkins();
            UpdateSpringSkins();
            UpdateSpins();
        }

        private void UpdatePlayerSkins()
        {
            if (!PlayerProgress.SkinsUnlocked.Contains(PlayerFactory.SkinDefault))
            {
                PlayerProgress.SkinsUnlocked =
                    PlayerProgress.SkinsUnlocked.Append(PlayerFactory.SkinDefault).ToArray();
            }

            if (PlayerProgress.TimesDied >= 50 && !PlayerProgress.SkinsUnlocked.Contains(PlayerFactory.SkinDied50))
            {
                PlayerProgress.SkinsUnlocked = PlayerProgress.SkinsUnlocked.Append(PlayerFactory.SkinDied50).ToArray();
            }

            if (PlayerProgress.LevelsTimes.Any(kvp => kvp.Value <= 30) &&
                !PlayerProgress.SkinsUnlocked.Contains(PlayerFactory.SkinLevelUnder30))
            {
                PlayerProgress.SkinsUnlocked =
                    PlayerProgress.SkinsUnlocked.Append(PlayerFactory.SkinLevelUnder30).ToArray();
            }
        }

        private void UpdateElevatorSkins()
        {
            Type elevator = typeof(ElevatorSkin);
            if (true)
                PlayerProgress.UnlockSkinFor(elevator, 0);
        }

        private void UpdateGateSkins()
        {
            Type gate = typeof(GateSkin);
            if (true)
                PlayerProgress.UnlockSkinFor(gate, 0);
        }

        private void UpdateSpikesSkins()
        {
            Type spikes = typeof(SpikesSkin);
            if (true)
                PlayerProgress.UnlockSkinFor(spikes, 0);
        }

        private void UpdateSpringSkins()
        {
            Type spring = typeof(SpringSkin);
            if (true)
                PlayerProgress.UnlockSkinFor(spring, 0);
        }

        public void UpdateSpins()
        {
            // Award a spin every 23 hours
            TimeSpan interval = DateTime.Now - PlayerProgress.RouletteSpinsLastUpdateTime;
            PlayerProgress.RouletteSpins += (int)interval.TotalHours / AWARD_SPIN_HOURS_INTERVAL;
            int remHours = (int)interval.TotalHours % AWARD_SPIN_HOURS_INTERVAL;
            DateTime date = DateTime.Now.AddHours(remHours).AddMinutes(interval.Minutes).AddSeconds(interval.Seconds);
//#if UNITY_IOS
//            LocalNotification notif = new LocalNotification
//            {
//                alertBody = "You have roulette spins available!",
//                fireDate = date
//            };
//            NotificationServices.ScheduleLocalNotification(notif);
//#endif
//#elif UNITY_ANDROID
//            AndroidJavaObject ajc = new AndroidJavaObject("com.zeljkosassets.notifications.Notifier");
//            ajc.CallStatic("sendNotification", Application.productName, "You have roulette spins available!",
//                "You have roulette spins available!", (DateTime.Now - date).TotalSeconds);
//#endif
        }
    }
}