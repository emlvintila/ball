﻿using Environment;
using Other;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class PlayerProgress
{
    public long Coins;

    public long TimesDied => Deaths.Sum(pair => pair.Value);

    public int CurrentSkin;

    public int RouletteSpins = 1;

    public DateTime RouletteSpinsLastUpdateTime = DateTime.Now;

    public List<KeyValuePair<string, float>> LevelsTimes = new List<KeyValuePair<string, float>>();

    public List<KeyValuePair<string, int>> SelectedEnvironmentalObjectsSkins = new List<KeyValuePair<string, int>>();

    public List<KeyValuePair<string, List<int>>> UnlockedEnvironmentalOBjectsSkins =
        new List<KeyValuePair<string, List<int>>>();

    public List<KeyValuePair<string, int>> Deaths = new List<KeyValuePair<string, int>>();

    public int[] SkinsUnlocked =
    { 
#if UNITY_EDITOR || DEVELOPMENT_BUILD || DEBUG
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20 
#endif
    };

    public float GetTimeForLevel(string level)
    {
        float time = LevelsTimes.FirstOrDefault(kvp => kvp.Key == level).Value;
        return time < 0.5f ? Mathf.Infinity : time;
    }

    public int GetSelectedSkinId(Type type)
    {
        if (!SelectedEnvironmentalObjectsSkins.Any())
            SelectSkinFor(type, 0);

        return SelectedEnvironmentalObjectsSkins.FirstOrDefault(pair => pair.Key == type.ToString()).Value;
    }

    public void UnlockSkinFor(Type type, int skin)
    {
        string key = type.ToString();
        KeyValuePair<string, List<int>> unlockedSkinsForType = UnlockedEnvironmentalOBjectsSkins.FirstOrDefault(pair => pair.Key == key);
        List<int> list = unlockedSkinsForType.Value;
        if (list == null)
        {
            list = new List<int>();
            UnlockedEnvironmentalOBjectsSkins.Remove(unlockedSkinsForType);

            KeyValuePair<string, List<int>> newPair = new KeyValuePair<string, List<int>>(key, list);
            UnlockedEnvironmentalOBjectsSkins.Add(newPair);
        }
        list.Add(skin);
    }

    public bool IsSkinUnlocked(Type type, int skin)
    {
        if (skin == 0)
            UnlockSkinFor(type, 0);
        string key = type.ToString();
        List<int> unlockedSkinsForType = UnlockedEnvironmentalOBjectsSkins.FirstOrDefault(pair => pair.Key == key).Value;

        return unlockedSkinsForType != null && unlockedSkinsForType.Contains(skin);
    }

    public void SelectSkinFor(Type type, int skin)
    {
        string key = type.ToString();

        if (!IsSkinUnlocked(type, skin))
            return;

        if (SelectedEnvironmentalObjectsSkins.Any(pair => pair.Key == key))
        {
            KeyValuePair<string, int> pairToRemove = SelectedEnvironmentalObjectsSkins.First(pair => pair.Key == key);
            SelectedEnvironmentalObjectsSkins.Remove(pairToRemove);
        }

        KeyValuePair<string, int> newPair = new KeyValuePair<string, int>(key, skin);
        SelectedEnvironmentalObjectsSkins.Add(newPair);
    }

    public int GetSelectedSkinId<T>()
    {
        return GetSelectedSkinId(typeof(T));
    }

    public void DiedTo(Type type)
    {
        string key = type == null ? "Environment" : type.ToString();

        KeyValuePair<string, int> d = Deaths.FirstOrDefault(pair => pair.Key == key);
        KeyValuePair<string, int> newPair = new KeyValuePair<string, int>(key, d.Value + 1);

        Deaths.Remove(d);
        Deaths.Add(newPair);
    }

    public static string TypeStringToFriendlyString(string typeString)
    {
        if (typeString == typeof(Spikes).ToString())
            return "Spikes";
        if (typeString == typeof(ShurikenController).ToString())
            return "Shuriken";

        return "Unknown";
    }

    public static string TypeToFriendlyString(Type type)
    {
        return TypeStringToFriendlyString(type.ToString());
    }

    private static readonly IReadOnlyDictionary<int, string> SkinTooltips = new Dictionary<int, string>
    {
        {
            0,
            "Gray"
        },
        {
            1,
            "Suicider\nDie 50 times"
        },
        {
            2,
            "Green"
        },
        {
            3,
            "Leaves"
        },
        {
            4,
            "This shouldn't be here"
        },
        {
            5,
            "Night Sky"
        },
        {
            6,
            "Lava"
        },
        {
            7,
            "Basketball"
        },
        {
            8,
            "Beachball"
        },
        {
            9,
            "Football"
        },
        {
            10,
            "Wheel"
        },
        {
            11,
            "Golf"
        },
        {
            12,
            "Wood"
        },
        {
            13,
            "Stone"
        },
        {
            14,
            "Christmas!"
        },
        {
            15,
            "Grass"
        },
        {
            16,
            "Pattern"
        }
    };

    private static readonly IReadOnlyDictionary<int, int> SkinCosts = new Dictionary<int, int>
    {
        {0, 0},
        {1, 0},
        {2, 0},
        {3, 100},
        {4, 200},
        {5, 300},
        {6, 400},
        {7, 500},
        {8, 600},
        {9, 700},
        {10, 800},
        {11, 900},
        {12, 1000},
        {13, 1100},
        {14, 1200},
        {15, 1300},
        {16, 1400}
    };

    public static string GetTooltipForSkin(int id)
    {
        return SkinTooltips.TryGetValue(id, out string result) ? result : "N/A";
    }

    public static int GetCostForSkin(int id)
    {
        return SkinCosts.TryGetValue(id, out int cost) ? cost : 0;
    }
}
