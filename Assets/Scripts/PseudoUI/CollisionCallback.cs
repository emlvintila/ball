﻿using UnityEngine;
using UnityEngine.Events;

namespace PseudoUI
{
    public class CollisionCallback : MonoBehaviour
    {
        public Collider other;

        public UnityEvent OnCollision;

        private void OnCollisionEnter(Collision collision)
        {
            if (other)
            {
                if (collision.collider == other)
                {
                    OnCollision.Invoke();
                }
            }
            else
            {
                OnCollision.Invoke();
            }
        }
    }
}
