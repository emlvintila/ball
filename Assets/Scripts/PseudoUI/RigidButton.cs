﻿using System.Collections;
using UnityEngine;

namespace PseudoUI
{
    public class RigidButton : MonoBehaviour
    {
        public float ForceMultiplier = 10f;

        public float SlingMultiplier = 0.15f;

        public float SlingDuration = 0.25f;

        public bool Interactable = true;

        [SerializeField]
        private GameObject button;

        [SerializeField]
        private GameObject ball;

#pragma warning disable CS0649
        [SerializeField]
        private Vector3 ballOffset;
#pragma warning restore CS0649

        private Rigidbody ballRigidbody;

        private Coroutine coroutine = null;

        private void Start()
        {
            if (!button)
            {
                button = transform.Find("Button").gameObject;
            }

            if (!ball)
            {
                ball = transform.Find("Ball").gameObject;
            }

            ballRigidbody = ball.GetComponent<Rigidbody>();
        }

        private void OnEnable()
        {
            foreach (Rigidbody rb in GetComponentsInChildren<Rigidbody>())
            {
                rb.velocity = Vector3.zero;
                rb.angularVelocity = Vector3.zero;
            }

            button.transform.localPosition = Vector3.zero;
            button.transform.localRotation = Quaternion.identity;
            ball.transform.localPosition = button.transform.localPosition + ballOffset;
            Interactable = true;
        }

        private void Reset()
        {
            button = transform.Find("Button").gameObject;
            ball = transform.Find("Ball").gameObject;
            Interactable = true;
        }

        public void PushBall()
        {
            if (Interactable)
            {
                Interactable = false;
                if (coroutine != null)
                {
                    StopCoroutine(coroutine);
                }

                coroutine = StartCoroutine(PushBallCoroutine());
            }
        }

        private IEnumerator PushBallCoroutine()
        {
            Vector3 direction = ForceMultiplier * (button.transform.position - ball.transform.position);
            Vector3 slingDirection = SlingMultiplier * -direction;
            ballRigidbody.AddForce(slingDirection, ForceMode.VelocityChange);
            yield return new WaitForSeconds(SlingDuration);
            ballRigidbody.AddForce(direction, ForceMode.VelocityChange);
            coroutine = null;
        }
    }
}
