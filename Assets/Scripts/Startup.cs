﻿using System.Collections;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Monetization;
using UnityEngine.SceneManagement;
#if UNITY_IOS
using UnityEngine.iOS;
using NotificationServices = UnityEngine.iOS.NotificationServices;
#endif

public class Startup : MonoBehaviour
{
#if UNITY_ANDROID
    private const string GAME_ID = "2900544";
#elif UNITY_IOS
    private const string GAME_ID = "2900545";
#else
    private const string GAME_ID = "";
#endif

    private const bool ADVERTISING_TEST_MODE = true;

    private void Awake()
    {
        Screen.autorotateToLandscapeLeft = true;
        Screen.autorotateToLandscapeRight = true;

        Time.timeScale = 1f;
        Time.fixedDeltaTime = 0.02f;

        Monetization.Initialize(GAME_ID, ADVERTISING_TEST_MODE);
#if !(UNITY_EDITOR || DEBUG || DEVELOPMENT_BUILD)
        StartCoroutine(ShowBannerCoroutine());
#endif

#if UNITY_IOS
        NotificationServices.RegisterForNotifications(NotificationType.Alert);
#endif
    }

    private IEnumerator ShowBannerCoroutine()
    {
        while (true)
        {
            string currentScene = SceneManager.GetActiveScene().name;
            if (currentScene == "Menu")
                Advertisement.Banner.Hide();
            else
                Advertisement.Banner.Show("banner");

            // wait until we switch to a new scene
            yield return new WaitUntil(() => SceneManager.GetActiveScene().name != currentScene);
        }
        // ReSharper disable once IteratorNeverReturns
    }
}
