﻿using Other;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.Events;
using UnityEngine.Monetization;

namespace UI
{
    public class AdEnabledButton : MonoBehaviour
    {
        public string PlacementId = "video";

        public float AdInterval = 120;

        public bool ShouldShowAd =>
            Time.realtimeSinceStartup - LastAdTime > AdInterval && ((ShowOnce && !shown) || !ShowOnce);

        public bool ShowOnce;

        public bool DisableIfNoAd;

        public static float LastAdTime = 0f;

        public UnityEvent AfterAdFinished;

        public UnityEvent AfterAdCompletedSuccessfully;

        public AnalyticsEventTracker AdStartEvent;

        public AnalyticsEventTracker AdSkippedEvent;

        public AnalyticsEventTracker AdCompleteEvent;

        private bool shown = false;

        protected void Awake()
        {
            if (AfterAdFinished == null)
                AfterAdFinished = new UnityEvent();
            if (AfterAdCompletedSuccessfully == null)
                AfterAdCompletedSuccessfully = new UnityEvent();

            AfterAdFinished.AddListener(() => shown = true);

            if (Mathf.Abs(LastAdTime) < Mathf2.Epsilon)
                LastAdTime = Time.realtimeSinceStartup;

            #if UNITY_EDITOR || DEBUG || DEVELOPMENT_BUILD
            AdInterval = float.PositiveInfinity;
            #endif
        }

        protected void Update()
        {
            if (DisableIfNoAd)
            {
                bool ready = Monetization.IsReady(PlacementId);
                if (gameObject.activeSelf)
                    if (!ready)
                        gameObject.SetActive(false);
                    else { }
                else
                    if (ready)
                        gameObject.SetActive(true);
            }

            if (ShowOnce && shown && gameObject.activeSelf)
                gameObject.SetActive(false);
        }

        public void TryShowAd()
        {
            if (ShouldShowAd)
                ShowAd();
            else
                AfterAdFinished.Invoke();
        }

        protected void ShowAd()
        {
            LastAdTime = Time.realtimeSinceStartup;
            if (Monetization.GetPlacementContent(PlacementId) is ShowAdPlacementContent ad)
                ad.Show(new ShowAdCallbacks { startCallback = AdStartCallback, finishCallback = AdResultCallback });
            else
                AfterAdFinished.Invoke();
        }

        protected virtual void AdStartCallback()
        {
            AdStartEvent?.TriggerEvent();
        }

        protected virtual void AdResultCallback(ShowResult result) 
        {
            if (result == ShowResult.Finished)
            {
                AdCompleteEvent?.TriggerEvent();
                AfterAdCompletedSuccessfully.Invoke();
            }

            if (result == ShowResult.Skipped)
                AdSkippedEvent?.TriggerEvent();
            AfterAdFinished.Invoke();
        }
    }
}
