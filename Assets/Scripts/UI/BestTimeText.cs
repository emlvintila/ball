﻿using System.Linq;
using Player;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Text))]
    public class BestTimeText : MonoBehaviour
    {
        public ProgressSaver ProgressSaver;

        public string Level;

        private Text text;

        private void Start()
        {
            text = GetComponent<Text>();
            if (!ProgressSaver)
                ProgressSaver = FindObjectOfType<ProgressSaver>();

            float bestTime = ProgressSaver.PlayerProgress.LevelsTimes.FirstOrDefault(kvp => kvp.Key == Level).Value;
            text.text = float.IsInfinity(bestTime) || bestTime < 0.5f ? "N/A" : bestTime.ToString("F1") + "s";
        }
    }
}
