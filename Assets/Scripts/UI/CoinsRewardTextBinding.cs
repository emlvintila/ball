﻿using Level;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Text))]
    public class CoinsRewardTextBinding : MonoBehaviour
    {
        [SerializeField]
        private LevelCoinsReward levelCoinsReward;

        private Text text;

        private void Awake()
        {
            text = GetComponent<Text>();
        }

        private void Update()
        {
            text.text = "+" + levelCoinsReward.Reward.ToString();
        }

        private void Reset()
        {
            if (!levelCoinsReward)
                levelCoinsReward = GetComponentInParent<LevelCoinsReward>();
        }
    }
}
