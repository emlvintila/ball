﻿using Player;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Text))]
    public class CoinsTextBinding : MonoBehaviour
#if DEVELOPMENT_BUILD || DEBUG || UNITY_EDITOR
    , IPointerDownHandler
#endif
    {
        private Text text;

        private ProgressSaver progressSaver;

        private void Awake()
        {
            text = GetComponent<Text>();
            progressSaver = FindObjectOfType<ProgressSaver>(); 
        }

        private void Update()
        {
            text.text = progressSaver.PlayerProgress.Coins.ToString();
        }

#if DEVELOPMENT_BUILD || DEBUG || UNITY_EDITOR
        public void OnPointerDown(PointerEventData eventData)
        {
            if (progressSaver.PlayerProgress.Coins <= 0)
                progressSaver.PlayerProgress.Coins = 100;
            else
                progressSaver.PlayerProgress.Coins *= 2;
        }
#endif
    }
}
