﻿using Menu;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Text))]
    public class EnvironmentalObjectSkinUnlockerTextBinding : MonoBehaviour
    {
        private Text text;

        private void Awake()
        {
            text = GetComponent<Text>();
            text.text = GetComponentInParent<EnvironmentalObjectSkinUnlocker>().CoinsNeededToUnlock.ToString();
        }
    }
}
