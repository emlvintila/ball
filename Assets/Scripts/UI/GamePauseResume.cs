﻿using Player;
using UnityEngine;
using UnityEngine.Events;

namespace UI
{
    public class GamePauseResume : MonoBehaviour
    {
        public UnityEvent OnGamePaused;

        public UnityEvent OnGameResumed;

        private PlayerMovement playerMovement;

        private void Start()
        {
            if (OnGamePaused == null)
                OnGamePaused = new UnityEvent();
            if (OnGameResumed == null)
                OnGameResumed = new UnityEvent();

            if (!playerMovement)
                playerMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();

            OnGamePaused.AddListener(DisablePlayerMovement);
            OnGameResumed.AddListener(EnablePlayerMovement);
        }

        private void DisablePlayerMovement()
        {
            playerMovement.enabled = false;
        }

        private void EnablePlayerMovement()
        {
            playerMovement.enabled = true;
            playerMovement.ResetMouse();
            //playerMovement.GetComponent<Rigidbody>().WakeUp();
            //Input.ResetInputAxes();
        }

        public void Pause()
        {
            if (!playerMovement.Alive)
                return;

            Time.timeScale = 0f;
            Time.fixedDeltaTime = 0f;
            OnGamePaused.Invoke();
        }

        public void Resume()
        {
            if (!playerMovement.Alive)
                return;

            Time.timeScale = 1f;
            Time.fixedDeltaTime = 0.02f;
            OnGameResumed.Invoke();
        }
    }
}
