﻿using System;
using System.Linq;
using Level;
using Player;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class LevelSelectorStarDisplay : MonoBehaviour
    {
#pragma warning disable IDE0044, CS0649
        [SerializeField]
        private string level;

        [SerializeField]
        private Sprite emptyStar;

        [SerializeField]
        private Sprite fullStar;

        [SerializeField]
        private Image[] stars;
#pragma warning restore IDE0044, CS0649

        [SerializeField]
        private ProgressSaver progressSaver;

        private void Start()
        {
            if (!progressSaver)
                progressSaver = FindObjectOfType<ProgressSaver>();

            float time = progressSaver.PlayerProgress.LevelsTimes.
                FirstOrDefault(kvp => kvp.Key == level).Value;
            if (time < 0.5f)
                time = Mathf.Infinity;
            float maxTime = (float) (LevelTimeReference) Enum.Parse(typeof(LevelTimeReference), level);

            float emptyStarsFloat = (time / maxTime - 1) * stars.Length;
            int emptyStars = Mathf.Clamp(float.IsInfinity(emptyStarsFloat) ? stars.Length : (int) emptyStarsFloat,
                0, stars.Length);
            int fullStars = stars.Length - emptyStars;

            for (int i = 0; i < emptyStars; ++i)
                stars[i].sprite = emptyStar;

            for (int i = emptyStars; i < fullStars; ++i)
                stars[i].sprite = fullStar;
        }
    }
}
