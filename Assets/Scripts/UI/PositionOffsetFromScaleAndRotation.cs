﻿using Other;
using UnityEngine;

namespace UI
{
    [ExecuteInEditMode]
    public class PositionOffsetFromScaleAndRotation : MonoBehaviour
    {
        public Vector3 Multiplier = Vector3.one;

        public Transform Transform;

        private void Awake()
        {
            if (!Transform)
                Transform = transform;
        }

        private void Update()
        {
            Vector3 newPosition = transform.localPosition;
            newPosition.x = Transform.localScale.x * Multiplier.x *
                            Mathf.Sin(Transform.localRotation.eulerAngles.y * Mathf.Deg2Rad);
            newPosition.y = Transform.localScale.y * Multiplier.y *
                            Mathf.Sin(Transform.localRotation.eulerAngles.z * Mathf.Deg2Rad);
            newPosition.z = Transform.localScale.z * Multiplier.z *
                            Mathf.Sin(Transform.localRotation.eulerAngles.x * Mathf.Deg2Rad);

            transform.localPosition = newPosition;
        }
    }
}
