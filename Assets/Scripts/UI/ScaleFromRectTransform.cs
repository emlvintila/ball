using UnityEngine;

namespace UI
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(RectTransform))]
    public class ScaleFromRectTransform : MonoBehaviour
    {
        public RectTransform RectTransform;

        public Vector2 Multiplier = Vector2.one;

        public Vector2 Delta;

        public float AspectRatio = 1;

        private RectTransform selfRectTransform;

        private SphereCollider sphereCollider;

        private void Awake()
        {
            if (!RectTransform)
                RectTransform = GetComponent<RectTransform>();
            selfRectTransform = GetComponent<RectTransform>();
            sphereCollider = GetComponent<SphereCollider>();
        }

        private void Update()
        {
            Vector3 scale = RectTransform.localScale;
            scale.x = Multiplier.x * RectTransform.rect.width + Delta.x;
            scale.y = Multiplier.y * RectTransform.rect.height + Delta.y;
            scale.x = Mathf.Min(scale.x, scale.y);
            scale.y = AspectRatio * scale.x;
            scale.z = (scale.x + scale.y) / 2;
            selfRectTransform.localScale = scale;

            if (sphereCollider)
                sphereCollider.radius = 0.5f * (Mathf.Min(scale.x, scale.y) / Mathf.Max(scale.x, scale.y));
        }
    }
}
