﻿using System;
using System.Collections.Generic;
using Level;
using Player;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(Text))]
    public class TotalNumberOfStars : MonoBehaviour
    {
        private const int MAX_NUMBER_OF_STARS = 5;

        [SerializeField]
        private ProgressSaver progressSaver;

        private Text text;

        private int numberOfStars = 0;

        private void Start()
        {
            text = GetComponent<Text>();
            if (!progressSaver)
                progressSaver = FindObjectOfType<ProgressSaver>();

            List<KeyValuePair<string, float>> times = progressSaver.PlayerProgress.LevelsTimes;
            foreach (KeyValuePair<string, float> kvp in times)
            {
                float time = kvp.Value;
                if (time < 0.5f)
                    time = Mathf.Infinity;
                float maxTime = (float)(LevelTimeReference)Enum.Parse(typeof(LevelTimeReference), kvp.Key);

                float emptyStarsFloat = (time / maxTime - 1) * MAX_NUMBER_OF_STARS;
                int emptyStars = Mathf.Clamp(float.IsInfinity(emptyStarsFloat) ? MAX_NUMBER_OF_STARS : (int)emptyStarsFloat,
                    0, MAX_NUMBER_OF_STARS);
                int fullStars = MAX_NUMBER_OF_STARS - emptyStars;

                numberOfStars += fullStars;
            }

            text.text = numberOfStars.ToString();
        }
    }
}
